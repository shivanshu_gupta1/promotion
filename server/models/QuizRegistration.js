const mongoose = require('mongoose');
// Schema
const quizregistrationsDataSchema = new mongoose.Schema({
    quizId: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    browser: {
        type: Object,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})
const quizregistrations = mongoose.model('quizregistrations', quizregistrationsDataSchema)
module.exports = quizregistrations