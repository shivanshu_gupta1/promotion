const mongoose = require('mongoose');
// Schema
const businessDataSchema = new mongoose.Schema({
    businessname: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    zipcode: {
        type: String,
        required: false
    },
    businesscategory: {
        type: String,
        required: false
    },
    businessphoneno: {
        type: String,
        required: false
    },
    website: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: false
    },
    plan: {
        type: String,
        required: false
    },
    paid: {
        type: Boolean,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})
const business = mongoose.model('business', businessDataSchema)
module.exports = business