const mongoose = require('mongoose');
// Schema
const quizquestionsDataSchema = new mongoose.Schema({
    quizId: {
        type: String,
        required: false
    },
    question: {
        type: String,
        required: false
    },
    option1: {
        type: String,
        required: false
    },
    option2: {
        type: String,
        required: false
    },
    option3: {
        type: String,
        required: false
    },
    option4: {
        type: String,
        required: false
    },
    answers: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})
const quizquestions = mongoose.model('quizquestions', quizquestionsDataSchema)
module.exports = quizquestions