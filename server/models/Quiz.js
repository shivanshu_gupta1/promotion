const mongoose = require('mongoose');
// Schema
const quizsDataSchema = new mongoose.Schema({
    title: {
        type: String,
        required: false
    },
    quizId: {
        type: String,
        required: false
    },
    userId: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    buttonlabel: {
        type: String,
        required: false
    },
    registration_description: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})
const quizs = mongoose.model('quizs', quizsDataSchema)
module.exports = quizs