const mongoose = require('mongoose');
// Schema
const emailmarketingsDataSchema = new mongoose.Schema({
    campaignName: {
        type: String,
        required: false
    },
    userId: {
        type: String,
        required: false
    },
    to: {
        type: String,
        required: false
    },
    from: {
        type: String,
        required: false
    },
    subject: {
        type: String,
        required: false
    },
    designcontent: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})
const emailmarketings = mongoose.model('emailmarketings', emailmarketingsDataSchema)
module.exports = emailmarketings