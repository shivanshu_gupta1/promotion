import firebase from "firebase/app";
import "firebase/storage";
import "firebase/database";
import "firebase/auth";
import "firebase/messaging";
import "firebase/analytics";
import 'firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyB8hxwhsvBFcXlHWNeqmGvm123Oi7_ZR3w",
    authDomain: "my-biz-promos.firebaseapp.com",
    projectId: "my-biz-promos",
    storageBucket: "my-biz-promos.appspot.com",
    messagingSenderId: "816728582719",
    appId: "1:816728582719:web:1bd9cd72ee16fe7a65e68a",
    measurementId: "G-0HFLJ6FDVH"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


const storage = firebase.storage();
const database = firebase.database();
const auth = firebase.auth();
const firestore = firebase.firestore();

// Authentication for Google
var googleProvider = new firebase.auth.GoogleAuthProvider();
// Authentication for Facebook
var facebookProvider = new firebase.auth.FacebookAuthProvider();
// Authentication for Twitter
var twitterProvider = new firebase.auth.TwitterAuthProvider();

export {
    firestore, auth, googleProvider, facebookProvider, twitterProvider, database, storage, firebase as default
}