import React from 'react';
import { API_SERVICE } from '../config/URI';
import queryString from 'query-string';
import axios from 'axios';
import Profile from './static/Profile';

const PromotionList = ({ promotion, i, settotalleads }) => {
    settotalleads(i);
    return (
        <div>
            <h6>{i}</h6>
            <br />
            <h5>First Name: {promotion.firstname}</h5>
            <hr />
            <h5>Last Name: {promotion.lastname}</h5>
            <hr />
            <h5>Email: {promotion.email}</h5>
            <hr />
            <h5>Browser: {promotion.browser.name}</h5>
            <hr />
            <h5>Version: {promotion.browser.version}</h5>
            <hr />
            <h5>OS: {promotion.browser.os}</h5>
            <hr />
        </div>
    )
}

const Viewpromotion = ({ location }) => {
    const uid = "XYZ00";
    const [allpromotions, setallpromotions] = React.useState([]);
    const [title, settitle] = React.useState('');
    const [promotionId, setpromotionId] = React.useState('');
    const [totalleads, settotalleads] = React.useState('');

    React.useEffect(() => {
        const { i } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${i}`)
            .then((d) => {
                var name = d.data[0].title;
                name = name.substring(0, 28);
                // Remove HTML Elements from string
                name = name.replace( /(<([^>]+)>)/ig, '');
                // Remove Icons from string
                name = name.replace(/[^\x20-\x7E]/g, '');
                settitle(name);
            }).catch(err => console.log(err));

        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetailsviews/${i}`)
            .then((d) => {
                setallpromotions(d.data);
            }).catch(err => console.log(err));


        setpromotionId(i);
    }, []);

    const showPromotionsAll = () => {
        var i = 0;
        return allpromotions.map(promotion => {
            i = i + 1;
            return <PromotionList settotalleads={settotalleads} i={i} promotion={promotion} key={promotion._id} />
        })
    }

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">
                                {title}...
                                <br />
                                Total Leads {totalleads}
                                <br />
                                14 Days Left to expire
                            </h1>
                            <buuton className="btn btn-danger">Deactivate Now</buuton>
                            </div>
                            <div class="row no-gutters mx-n2 mb-4">
                            <div class="card">
                            <div class="card-body">
                                {showPromotionsAll()}
                            </div>
                            </div>
                            </div>
                            <div class="d-sm-flex align-items-center text-center text-sm-start">
                            <h6 class="text-nowrap my-2 me-3">Share this list:</h6><a class="btn-social bs-facebook me-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter me-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-email me-2 my-2" href="#"><i class="ai-mail"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}


export default Viewpromotion