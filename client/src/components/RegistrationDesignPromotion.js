import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import parse from 'html-react-parser';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
}));

const RegistrationDesignPromotion = ({ location }) => {
    const classes = useStyles();
    const [description, setdescription] = React.useState(`The company responsible for processing your data is [...............] located in [...............], and identified with [...............]. The data provided in the contest will not be passed to any third party. You can access, edit and delete your data, or oppose certain uses of the same, oppose transfer or exercise the right to be forgotten by sending an email to [...............]. More information in the Privacy Policy.`);
    const [promotionId, setpromotionId] = React.useState('');
    const [update, setupdate] = React.useState(false);

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        setpromotionId(q);
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${q}`)
            .then((d) => {
                setdescription(d.data[0].registration_description);
            }).catch(err => console.log(err));
    }, []);

    const [state, setState] = React.useState({
        left: false
      });
    
    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-designpromotion?q=${promotionId}`} button href="/designpromotion">
                <ListItemText primary='Create/Design Template' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-registrationdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Registration Form' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-thankyoudesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Thank You' />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-datesdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Dates' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-leagaldesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Leagal Content' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-sharedesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Share' />
            </ListItem>
          </List>
        </div>
    );

    const refreshPromotion = () => {
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${promotionId}`)
            .then((d) => {
                setdescription(d.data[0].registration_description);
            }).catch(err => console.log(err));
    }

    const savePromotion = () => {
        var uploadData = {
            registration_description: description,
            promotionId
        }
        axios.post(`${API_SERVICE}/api/v1/main/savepromotion2`, uploadData)
            .then(() => {
                setupdate(true);
                refreshPromotion();
            }).catch(err => console.log(err));
    }

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <div className={classes.root}>
                    {
                        update ? (
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong> Your Promotion is successfully saved
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ) : null
                    }
                    <Grid container spacing={3}>
                            <Grid item xs={4}>
                                {['left'].map((anchor) => (
                                <React.Fragment key={anchor}>
                                <Button startIcon={<MoreHorizIcon />} fullWidth style={{ backgroundColor: '#fff', color: '#000', marginBottom: '4px' }} variant="contained" onClick={toggleDrawer(anchor, true)}>More Options</Button>
                                <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                                    {list(anchor)}
                                </Drawer>
                                </React.Fragment>
                            ))}
                            <Paper className={classes.paper}>
                                <Button onClick={savePromotion} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                    Save
                                </Button>
                                <br />
                                <h3 className="float-left text-dark">Description</h3>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    data={description}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setdescription(data);
                                    } }
                                />
                                <hr />
                            </Paper>    
                        </Grid>
                        <Grid className="stick" item xs={8}>
                            <Paper className={classes.paper}>
                                <section className="borderlogin">
                                    <center>
                                        <img className="imagebanner" src="https://blog.resellerclub.com/wp-content/uploads/2015/07/1.png" />
                                    </center>
                                    <br />
                                    <h4 style={{ float: 'left' }}>First Name</h4>
                                    <TextField fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Last Name</h4>
                                    <TextField fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Email</h4>
                                    <TextField fullWidth id="outlined-basic" type="email" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Password</h4>
                                    <TextField fullWidth id="outlined-basic" type="password" variant="outlined" />
                                    <br />
                                    <br />
                                    <Button fullWidth size="large" variant="contained" color="primary">
                                        Register
                                    </Button>
                                    <br />
                                    <br />
                                    <div className="title">
                                        {parse(`
                                            ${description}
                                        `)}
                                    </div>
                                </section>
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
                </div>
                </main>
        </>
    )
}

export default RegistrationDesignPromotion
