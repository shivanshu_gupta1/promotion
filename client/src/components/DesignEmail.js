import React, { useRef } from 'react';
import EmailEditor from 'react-email-editor';
import styled from 'styled-components';
import sample from './static/sample.json';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import EmailIcon from '@material-ui/icons/Email';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
  margin-left: 20px;
  margin-right: 20px;
`;

const Bar = styled.div`
  flex: 1;
  background-color: #61dafb;
  color: #000;
  padding: 10px;
  display: flex;
  max-height: 40px;
  h1 {
    flex: 1;
    font-size: 16px;
    text-align: left;
  }
  button {
    flex: 1;
    padding: 10px;
    margin-left: 10px;
    font-size: 14px;
    font-weight: bold;
    background-color: #000;
    color: #fff;
    border: 0px;
    max-width: 150px;
    cursor: pointer;
  }
`;


const DesignEmail = ({ location }) => {
    const uid = sessionStorage.getItem("userId");

    const emailEditorRef = useRef(null);
    const [campaignId, setcampaignId] = React.useState('');
    const [campaign, setcampaign] = React.useState({});
    const [design, setdesign] = React.useState('');

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchaemailcampign/${q}`)
            .then((d) => {
                setcampaign(d.data[0]);
            }).catch(err => console.log(err));
        setcampaignId(q);
    }, []);

    const saveDesign = () => {
        emailEditorRef.current.editor.saveDesign((design) => {
        console.log('saveDesign', design);
        alert('Design JSON has been logged in your developer console.');
        });
    };

    const exportHtml = () => {
        emailEditorRef.current.editor.exportHtml((data) => {
            const { design, html } = data;
            var uploadData = {
                campaignId,
                designcontent: html
            }
            axios.post(`${API_SERVICE}/api/v1/main/savecampaignemaildesign`, uploadData)
                .then(() => {
                    window.close();
                }).catch(err => console.log(err));
        });
    };

    const onDesignLoad = (data) => {
        console.log('onDesignLoad', data);
    };

    const onLoad = () => {
        emailEditorRef.current.editor.addEventListener(
        'onDesignLoad',
        onDesignLoad
        );
        emailEditorRef.current.editor.loadDesign(sample);
    };

   

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <Container>
                    <div>
                        <Button startIcon={<EmailIcon />} size="large" color="primary" style={{  float: 'right', marginLeft: '4px', marginTop: '4px', marginBottom: '4px' }} variant="contained" >Back to Campaign</Button>
                        <Button startIcon={<SaveIcon />} onClick={exportHtml} size="large" style={{  backgroundColor: '#fff', color: '#000', float: 'right', marginTop: '4px', marginBottom: '4px' }} variant="contained" >Save Design</Button>
                    </div>
                    <React.StrictMode>
                        <EmailEditor style={{ height: '88vh' }} ref={emailEditorRef} onLoad={onLoad} />
                    </React.StrictMode>
                </Container>
                </div>
                </main>
        </>
    )
}

export default DesignEmail