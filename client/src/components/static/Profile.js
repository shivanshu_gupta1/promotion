import React from 'react'
import { auth } from '../../Firebase/index';
const Profile = () => {
    const [user, setUser] = React.useState({});
    const [fullName, setFullName] = React.useState('');
    const [email, setEmail] = React.useState('');

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
            if (user) {
                setUser(user);
                setFullName(user.displayName);
                setEmail(user.email);
            } else {
                console.log("No");
            }
        });
    }, []);

    const signOut = () => {
        auth.signOut().then(() => {
            window.location.href = "/";
        }).catch((error) => {
            // An error happened.
        });
    }


    return (
        <>
            <div class="col-lg-4 mb-4 mb-lg-0">
                <div class="bg-light rounded-3 shadow-lg">
                <div class="px-4 py-4 mb-1 text-center"><img class="d-block rounded-circle mx-auto my-2" src={user.photoURL} at="Shivanshu Gupta" width="110" />
                    <h6 class="mb-0 pt-1">{fullName}</h6><span class="text-muted fs-sm">{email}</span>
                </div>
                <div class="d-lg-block collapse pb-2" id="account-menu">
                    <h3 class="d-block bg-secondary fs-sm fw-semibold text-muted mb-0 px-4 py-3">Menu</h3><a class="d-flex align-items-center nav-link-style px-4 py-3" href="/marketing-tool-dashboardhome">Home</a>
                    <a onClick={signOut} class="d-flex align-items-center nav-link-style px-4 py-3 border-top" href="signin-illustration.html"><i class="ai-log-out fs-lg opacity-60 me-2"></i>Sign out</a>
                </div>
                </div>
            </div>
        </>
    )
}

export default Profile
