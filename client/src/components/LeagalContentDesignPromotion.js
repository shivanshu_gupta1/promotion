import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import parse from 'html-react-parser';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import queryString from 'query-string';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
}));

const LeagalContentDesignPromotion = ({ location }) => {
    const classes = useStyles();
    const [title, settitle] = React.useState('✅ Enter your code and [TRY YOUR LUCK/ENTER THE PRIZE DRAW] 🍀');
    const [label, setlabel] = React.useState('Label');
    const [promotionId, setpromotionId] = React.useState('');
    const [description, setdescription] = React.useState(`
        [COMPLETE THIS TERMS AND CONDITIONS TEMPLATE OR COPY AND PASTE YOUR OWN HERE] NO PURCHASE IS NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT INCREASE THE CHANCES OF WINNING. 1. Eligibility: The contest (the “Contest”) is open only to those who sign up on the online contest page and who are [age] as of the date of entry. The contest is only open to legal residents of [eligible countries] and is void where prohibited by law. Employees of [company name] their respective affiliates, subsidiaries, advertising and promotion agencies, suppliers and their immediate family members and/or those living in the same household of each are not eligible to participate in the contest. The contest is subject to all applicable federal, state and local laws and regulations. Void where prohibited. 
    `);

    const [state, setState] = React.useState({
        left: false
      });
    
    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        setpromotionId(q);
    }, []);

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
        <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-designpromotion?q=${promotionId}`} button href="/designpromotion">
                <ListItemText primary='Create/Design Template' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-registrationdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Registration Form' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-thankyoudesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Thank You' />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-datesdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Dates' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-leagaldesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Leagal Content' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-sharedesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Share' />
            </ListItem>
          </List>
        </div>
    );

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <div className={classes.root}>
                    
                    <Grid container spacing={3}>
                            <Grid item xs={4}>
                                {['left'].map((anchor) => (
                                <React.Fragment key={anchor}>
                                <Button startIcon={<MoreHorizIcon />} fullWidth style={{ backgroundColor: '#fff', color: '#000', marginBottom: '4px' }} variant="contained" onClick={toggleDrawer(anchor, true)}>More Options</Button>
                                <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                                    {list(anchor)}
                                </Drawer>
                                </React.Fragment>
                            ))}
                            <Paper className={classes.paper}>
                                <Button startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                    Save
                                </Button>
                                <br />
                                <h3 className="float-left text-dark">Terms & Conditions</h3>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    data={description}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setdescription(data);
                                    } }
                                />
                                <hr />
                                <h3 className="float-left text-dark">Button Label</h3>
                                <TextField fullWidth id="outlined-basic" value={label} onChange={(e) => setlabel(e.target.value)} variant="outlined" />
                            </Paper>    
                        </Grid>
                        <Grid className="stick" item xs={8}>
                            <Paper className={classes.paper}>
                                <section className="borderlogin">
                                    <center>
                                        <b>TERMS & CONDITIONS</b>
                                    </center>
                                    <div className="title">
                                        {parse(`
                                            ${description}
                                        `)}
                                    </div>
                                    <br />
                                </section>
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
                </div>
                </main>
        </>
    )
}

export default LeagalContentDesignPromotion
