import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Profile from './static/Profile';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
    appBar: {
      position: 'relative',
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
}));

const Email = ({ location }) => {
    const uid = sessionStorage.getItem("userId");
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [section, setsection] = React.useState(1);
    const [campaign, setcampaign] = React.useState({});
    const [campaignId, setcampaignId] = React.useState('');
    const [to, setto] = React.useState('');
    const [from, setfrom] = React.useState('');
    const [subject, setsubject] = React.useState('');



    const handleClickOpen = (section) => {
      setOpen(true);
      setsection(section);
    };
    const handleClose = () => {
      setOpen(false);
    };

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchaemailcampign/${q}`)
            .then((d) => {
                setcampaign(d.data[0]);
            }).catch(err => console.log(err));
        setcampaignId(q);
    }, []);

    const saveCampaign = () => {
        if (section === 1) {
            var uploadData = {
                to,
                campaignId
            }
            axios.post(`${API_SERVICE}/api/v1/main/savecampaign1`, uploadData)
                .then(() => {
                    handleClose();
                }).catch(err => console.log(err));
        } else if (section === 2) {
            var uploadData = {
                from,
                campaignId
            }
            axios.post(`${API_SERVICE}/api/v1/main/savecampaign2`, uploadData)
                .then(() => {
                    handleClose();
                }).catch(err => console.log(err));
        } else if (section === 3) {
            var uploadData = {
                subject,
                campaignId
            }
            axios.post(`${API_SERVICE}/api/v1/main/savecampaign3`, uploadData)
                .then(() => {
                    handleClose();
                }).catch(err => console.log(err));
        }
    }

    return (
        <>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                    <CloseIcon />
                    </IconButton>
                </Toolbar>
                </AppBar>
                <Container style={{ marginTop: '4%' }}>
                    {
                        section === 1 ? (
                            <>
                            <div class="form-floating mb-3">
                                <input value={to} onChange={(e) => setto(e.target.value)} class="form-control" type="text" id="fl-text" placeholder="To" />
                                <label for="fl-text">To</label>
                            </div>
                            <button onClick={saveCampaign} className="btn btn-lg w-100 btn-primary">Save</button>
                            </>
                        ) : section === 2 ? (
                            <>
                            <div class="form-floating mb-3">
                                <input value={from} onChange={(e) => setfrom(e.target.value)} class="form-control" type="email" id="fl-text" placeholder="example@mail.com" />
                                <label for="fl-text">From</label>
                            </div>
                            <button onClick={saveCampaign} className="btn btn-lg w-100 btn-primary">Save</button>
                            </>
                        ) : section === 3 ? (
                            <>
                            <div class="form-floating mb-3">
                                <input value={subject} onChange={(e) => setsubject(e.target.value)} class="form-control" type="text" id="fl-text" placeholder="Email Subject" />
                                <label for="fl-text">Subject</label>
                            </div>
                            <button onClick={saveCampaign} className="btn btn-lg w-100 btn-primary">Save</button>
                            </>
                        ) :  null
                    }
                </Container>
            </Dialog>

            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">{campaign.campaignName}</h1>
                            <a href={`/marketing-tool-view?q=${campaignId}`} target="_blank" className="btn btn-primary">
                                View Template
                            </a>
                            </div>
                            <div>
                                <div className="card card-hover mt-2">
                                    <div class="card-body">
                                        <h4 class="card-title">To</h4>
                                        <p class="card-text">All subscribed contacts in the audience. Your 'To' field is not personalized with merge tags.</p>
                                        <a href="#" onClick={() => handleClickOpen(1)} class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>

                                <div className="card card-hover mt-2">
                                    <div class="card-body">
                                        <h4 class="card-title">From</h4>
                                        <p class="card-text">Please insert the email address used to send the mail from.</p>
                                        <a href="#" onClick={() => handleClickOpen(2)} class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>

                                <div className="card card-hover mt-2">
                                    <div class="card-body">
                                        <h4 class="card-title">Subject</h4>
                                        <p class="card-text">What's the subject line for this campaign?</p>
                                        <a href="#" onClick={() => handleClickOpen(3)} class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>

                                <div className="card card-hover mt-2">
                                    <div class="card-body">
                                        <h4 class="card-title">Content</h4>
                                        <p class="card-text">Design the content for your email.</p>
                                        <a href="#!" onClick={() => window.open(`/marketing-tool-designemail?q=${campaignId}`, '_blank', 'location=yes,height=900,width=1200,scrollbars=yes,status=yes')} class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-lg w-100 mt-4 btn-success">Send</button>
                                


                            </div>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default Email