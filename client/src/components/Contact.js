import React from 'react'
import Header from './static/Header';
import Footer from './static/Footer';

const Contact = () => {
    return (
        <>
            <main className="page-wrapper">
                <Header />
                    <h2 class="mb-5 mt-5 text-center">Contact Us</h2>
                    <div className="mt-5 mb-5 container w-50">
                        <div class="form-floating mb-3">
                        <input class="form-control" type="text" id="fl-text" placeholder="Your name" />
                        <label for="fl-text">Your name</label>
                        </div>
                        <div class="form-floating mb-3">
                        <input class="form-control" type="email" id="fl-text" placeholder="Your email" />
                        <label for="fl-text">Your email</label>
                        </div>
                        <div class="form-floating">
                        <textarea class="form-control" id="fl-textarea" style={{ height: '120px' }} placeholder="Your message"></textarea>
                        <label for="fl-textarea">Your message</label>
                        </div>
                        <button type="button" class="btn btn-primary mt-2 mb-2">Send Message</button>
                    </div>
                <Footer />
            </main>
        </>
    )
}

export default Contact
