import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import parse from 'html-react-parser';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import browserInfo from 'browser-info';
import QRCode from "react-qr-code";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      backgroundImage: 'url("https://pngimg.com/uploads/confetti/confetti_PNG86975.png")',
      backgroundRepeat: 'no-repeat',
      backgroundSize: '100%'
    },
}));

const minuteSeconds = 60;
const hourSeconds = 3600;
const daySeconds = 86400;

const timerProps = {
  isPlaying: true,
  size: 120,
  strokeWidth: 6
};

const renderTime = (dimension, time) => {
  return (
    <div className="time-wrapper">
      <div className="time">{time}</div>
      <div>{dimension}</div>
    </div>
  );
};

const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
const getTimeDays = (time) => (time / daySeconds) | 0;


const PublishPromo = ({ location }) => {
    const classes = useStyles();
    const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = stratTime + 243248; // use UNIX timestamp in seconds
    const remainingTime = endTime - stratTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;
    const [promotionId, setpromotionId] = React.useState('');
    const [steps, setsteps] = React.useState(1);

    const [firstname, setfirstname] = React.useState('');
    const [lastname, setlastname] = React.useState('');
    const [email, setemail] = React.useState('');
    const [password, setpassword] = React.useState('');


    React.useEffect(() => {
        const { s } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${s}`)
            .then((d) => {
                console.log(d);
                settitle(d.data[0].title);
                setdescription(d.data[0].description);
                setlabel(d.data[0].buttonlabel);
                setregistrationDescription(d.data[0].registration_description);
            }).catch(err => console.log(err));
        setpromotionId(s);
    }, []);

    const [title, settitle] = React.useState('✅ Enter your code and [TRY YOUR LUCK/ENTER THE PRIZE DRAW] 🍀');
    const [label, setlabel] = React.useState('Label');
    const [registrationDescription, setregistrationDescription] = React.useState('Label');
    const [description, setdescription] = React.useState(`
    <h3>Do you want to win a [DESCRIPTION OF PRIZE] valued at [PRICE]? </h3>
    
    ➡️ Follow these steps to participate:
    
    <ul><li>✏️Identify yourself and fill in the <strong>registration form</strong>.</li>
    <li>✅ <strong>Enter the code</strong> in the corresponding field to validate it.</li>
    <li>👍 Submit the form to finalize and you'll be in the [prize draw]</li></ul> 
    
    🗓️ Promotion valid until [INSERT DATES]`);

    const register = () => {
        var browser = browserInfo();
        var uploadData = {
            promotionId,
            firstname,
            lastname,
            email,
            password,
            browser
        }
        axios.post(`${API_SERVICE}/api/v1/main/registeruser`, uploadData)
            .then(() => {
                setsteps(3);
            }).catch(err => console.log(err));
    }

    return (
        <div className="container center promotionpublish">
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    {
                        steps === 1 ? (
                            <Paper className={classes.paper}>
                                <center>
                                    <img className="imagebanner" src="https://blog.resellerclub.com/wp-content/uploads/2015/07/1.png" />
                                </center>
                                <div style={{ color: '#000' }} className="title">
                                    {parse(`
                                        ${title}
                                    `)}
                                    <br />
                                    {parse(`
                                        ${description}
                                    `)}
                                </div>
                                <br />
                                <Button onClick={() => setsteps(2)} fullWidth size="large" variant="contained" color="primary">
                                    {label}
                                </Button>
                                <br />
                                <br />
                                <div className="countdown">
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#7E2E84"]]}
                                        duration={daysDuration}
                                        initialRemainingTime={remainingTime}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("days", getTimeDays(daysDuration - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#D14081"]]}
                                        duration={daySeconds}
                                        initialRemainingTime={remainingTime % daySeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > hourSeconds
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("hours", getTimeHours(daySeconds - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#EF798A"]]}
                                        duration={hourSeconds}
                                        initialRemainingTime={remainingTime % hourSeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > minuteSeconds
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("minutes", getTimeMinutes(hourSeconds - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#218380"]]}
                                        duration={minuteSeconds}
                                        initialRemainingTime={remainingTime % minuteSeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > 0
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("seconds", getTimeSeconds(elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                </div>
                                <br />
                                <br />
                                <a href="#!">
                                    TERMS & CONDITIONS
                                </a>
                            </Paper>
                        ) : steps === 2 ? (
                            <Paper className={classes.paper}>
                                <section>
                                    <center>
                                        <img className="imagebanner" src="https://blog.resellerclub.com/wp-content/uploads/2015/07/1.png" />
                                    </center>
                                    <br />
                                    <h4 style={{ float: 'left' }}>First Name</h4>
                                    <TextField onChange={(e) => setfirstname(e.target.value)} fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Last Name</h4>
                                    <TextField onChange={(e) => setlastname(e.target.value)} fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Email</h4>
                                    <TextField onChange={(e) => setemail(e.target.value)} fullWidth id="outlined-basic" type="email" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Password</h4>
                                    <TextField onChange={(e) => setpassword(e.target.value)} fullWidth id="outlined-basic" type="password" variant="outlined" />
                                    <br />
                                    <br />
                                    <Button onClick={register} fullWidth size="large" variant="contained" color="primary">
                                        Register
                                    </Button>
                                    <br />
                                    <br />
                                    <div className="title">
                                        {parse(`
                                            ${registrationDescription}
                                        `)}
                                    </div>
                                </section>
                            </Paper>
                        ) : steps === 3 ? (
                            <Paper className={classes.paper}>
                                <section>
                                    <center>
                                        <img className="imagebanner" src="https://blog.resellerclub.com/wp-content/uploads/2015/07/1.png" />
                                    </center>
                                    <div className="title">
                                        {parse(`
                                            <h1>Thank you for participating!</h1>
                                            <strong>
                                            You can share with your friends so they can enter too. 😀
                                            </strong>
                                        `)}
                                    </div>
                                    <br />
                                    <center>
                                        <QRCode value={promotionId} />
                                    </center>
                                    <br />
                                    <Button fullWidth size="large" variant="contained" color="secondary">
                                        Download
                                    </Button>
                                </section>
                            </Paper>
                        ) : null
                    }
                    
                </Grid>
            </Grid>
        </div>
    )
}

export default PublishPromo
