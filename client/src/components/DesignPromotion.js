import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import parse from 'html-react-parser';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';

const minuteSeconds = 60;
const hourSeconds = 3600;
const daySeconds = 86400;

const timerProps = {
  isPlaying: true,
  size: 120,
  strokeWidth: 6
};

const renderTime = (dimension, time) => {
  return (
    <div className="time-wrapper">
      <div className="time">{time}</div>
      <div>{dimension}</div>
    </div>
  );
};

const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
const getTimeDays = (time) => (time / daySeconds) | 0;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
}));

const CreatePromotion = ({ location }) => {
    const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = stratTime + 243248; // use UNIX timestamp in seconds
    const remainingTime = endTime - stratTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;

    const classes = useStyles();
    const userId = sessionStorage.getItem("userId");

    const [promotionId, setpromotionId] = React.useState('');
    const [update, setupdate] = React.useState(false);
    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${q}`)
            .then((d) => {
                settitle(d.data[0].title);
                setdescription(d.data[0].description);
                setlabel(d.data[0].buttonlabel);
            }).catch(err => console.log(err));
        setpromotionId(q);
    }, []);


    const [title, settitle] = React.useState('✅ Enter your code and [TRY YOUR LUCK/ENTER THE PRIZE DRAW] 🍀');
    const [label, setlabel] = React.useState('Label');
    const [description, setdescription] = React.useState(`
    <h3>Do you want to win a [DESCRIPTION OF PRIZE] valued at [PRICE]? </h3>
    
    ➡️ Follow these steps to participate:
    
    <ul><li>✏️Identify yourself and fill in the <strong>registration form</strong>.</li>
    <li>✅ <strong>Enter the code</strong> in the corresponding field to validate it.</li>
    <li>👍 Submit the form to finalize and you'll be in the [prize draw]</li></ul> 
    
    🗓️ Promotion valid until [INSERT DATES]`);

    const [state, setState] = React.useState({
        left: false
      });
    
    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
          <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-designpromotion?q=${promotionId}`} button href="/designpromotion">
                <ListItemText primary='Create/Design Template' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-registrationdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Registration Form' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-thankyoudesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Thank You' />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-datesdesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Dates' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-leagaldesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Leagal Content' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-sharedesignpromotion?q=${promotionId}`} button >
                <ListItemText primary='Share' />
            </ListItem>
          </List>
        </div>
    );

    const refreshPromotion = () => {
        axios.get(`${API_SERVICE}/api/v1/main/fetchpromotiondetails/${promotionId}`)
            .then((d) => {
                settitle(d.data[0].title);
                setdescription(d.data[0].description);
                setlabel(d.data[0].buttonlabel);
            }).catch(err => console.log(err));
    }

    const savePromotion = () => {
        var uploadData = {
            title,
            description,
            buttonlabel: label,
            promotionId
        }
        axios.post(`${API_SERVICE}/api/v1/main/savepromotion1`, uploadData)
            .then(() => {
                setupdate(true);
                refreshPromotion();
            }).catch(err => console.log(err));
    }

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <div className={classes.root}>
                    {
                        update ? (
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong> Your Promotion is successfully saved
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ) : null
                    }
                    
                    <Grid container spacing={3}>
                            
                            <Grid item xs={4}>
                                {['left'].map((anchor) => (
                                <React.Fragment key={anchor}>
                                <Button startIcon={<MoreHorizIcon />} fullWidth style={{ backgroundColor: '#fff', color: '#000', marginBottom: '4px' }} variant="contained" onClick={toggleDrawer(anchor, true)}>More Options</Button>
                                <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                                    {list(anchor)}
                                </Drawer>
                                </React.Fragment>
                            ))}
                            <Paper className={classes.paper}>
                                <Button onClick={savePromotion} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                    Save
                                </Button>
                                <br />
                                <h3 className="float-left text-dark">Title</h3>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    data={title}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        settitle(data);
                                    } }
                                />
                                <hr />
                                <h3 className="float-left text-dark">Description</h3>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    data={description}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setdescription(data);
                                    } }
                                />
                                <hr />
                                <h3 className="float-left text-dark">Button Label</h3>
                                <TextField fullWidth id="outlined-basic" value={label} onChange={(e) => setlabel(e.target.value)} variant="outlined" />
                            </Paper>    
                        </Grid>
                        <Grid className="stick" item xs={8}>
                            <Paper className={classes.paper}>
                                <section className="borderBanner">
                                    <center>
                                        <img className="imagebanner" src="https://blog.resellerclub.com/wp-content/uploads/2015/07/1.png" />
                                    </center>
                                    <div style={{ color: '#000' }} className="title">
                                        {parse(`
                                            ${title}
                                        `)}
                                        <br />
                                        {parse(`
                                            ${description}
                                        `)}
                                    </div>
                                    <br />
                                    <Button fullWidth size="large" variant="contained" color="primary">
                                        {label}
                                    </Button>
                                    <br />
                                    <br />
                                    <div className="countdown">
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#7E2E84"]]}
                                            duration={daysDuration}
                                            initialRemainingTime={remainingTime}
                                        >
                                            {({ elapsedTime }) =>
                                            renderTime("days", getTimeDays(daysDuration - elapsedTime))
                                            }
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#D14081"]]}
                                            duration={daySeconds}
                                            initialRemainingTime={remainingTime % daySeconds}
                                            onComplete={(totalElapsedTime) => [
                                            remainingTime - totalElapsedTime > hourSeconds
                                            ]}
                                        >
                                            {({ elapsedTime }) =>
                                            renderTime("hours", getTimeHours(daySeconds - elapsedTime))
                                            }
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#EF798A"]]}
                                            duration={hourSeconds}
                                            initialRemainingTime={remainingTime % hourSeconds}
                                            onComplete={(totalElapsedTime) => [
                                            remainingTime - totalElapsedTime > minuteSeconds
                                            ]}
                                        >
                                            {({ elapsedTime }) =>
                                            renderTime("minutes", getTimeMinutes(hourSeconds - elapsedTime))
                                            }
                                        </CountdownCircleTimer>
                                        <CountdownCircleTimer
                                            {...timerProps}
                                            colors={[["#218380"]]}
                                            duration={minuteSeconds}
                                            initialRemainingTime={remainingTime % minuteSeconds}
                                            onComplete={(totalElapsedTime) => [
                                            remainingTime - totalElapsedTime > 0
                                            ]}
                                        >
                                            {({ elapsedTime }) =>
                                            renderTime("seconds", getTimeSeconds(elapsedTime))
                                            }
                                        </CountdownCircleTimer>
                                    </div>
                                    <br />
                                    <br />
                                    <a href="#!">
                                        TERMS & CONDITIONS
                                    </a>
                                </section>
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
                </div>
                </main>
        </>
    )
}

export default CreatePromotion
