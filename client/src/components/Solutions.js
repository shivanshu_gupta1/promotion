import React from 'react';
import Header from './static/Header';
import Footer from './static/Footer';

const Solutions = () => {
    return (
        <>
        <main class="page-wrapper">
            <Header />
            <section class="container mt-4 pt-5 mt-md-0 pt-md-7 pb-5">
                <h2 class="mb-5 text-center">Our Solutions</h2>
                <div class="row">
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-user-check h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 1</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                    </div></a></div>
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-settings h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 2</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                    </div></a></div>
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-credit-card h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 3</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                    </div></a></div>
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-truck h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 4</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                    </div></a></div>
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-refresh-cw h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 5</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                    </div></a></div>
                <div class="col-lg-4 col-sm-6 mb-grid-gutter"><a class="card h-100 border-0 shadow card-hover" href="help-single-topic.html">
                    <div class="card-body ps-grid-gutter pe-grid-gutter text-center"><i class="ai-share-2 h2 text-primary mt-2 mb-4"></i>
                        <h3 class="h5">Solution 6</h3>
                        <p class="fs-sm text-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                     </div></a></div>
                </div>
            </section>
            <section class="container pt-md-4 mb-2 pb-5 pb-md-6 mb-md-0">
                <h2 class="mb-5 text-center">F.A.Q</h2>
                <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled">
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">How long will delivery take?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">What payment methods do you accept?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">Do you ship internationally?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">Do I need an account to place an order?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">How can I track my order?</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled">
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">What are the product refund conditions?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">Do you have discounts for returning customers?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">How do your referral program work?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">Where I can view and download invoices for my orders?</a></li>
                    <li class="d-flex align-items-center border-bottom pb-3 mb-3"><i class="ai-book text-muted me-2"></i><a class="nav-link-style" href="#">Do you provide technical support after the purchase?</a></li>
                    </ul>
                </div>
                </div>
            </section>
            </main>
            <Footer />
        </>
    )
}

export default Solutions
