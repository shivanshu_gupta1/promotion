import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Profile from './static/Profile';
import axios from 'axios';
import { API_SERVICE } from '../config/URI';



const EmailTemplateList = ({ allemailtemplate }) => {
    return (
        <div className="card text-left mt-2">
            <div className="card-body">
                <h4 className="card-title">{allemailtemplate.campaignName}</h4>
                <a target="_blank" href={`/marketing-tool-view?q=${allemailtemplate._id}`} className="btn btn-sm btn-primary">View</a>
            </div>
        </div>
    )
}

const AllEmail = () => {
    const uid = sessionStorage.getItem("userId");

    const [allemailtemplates, setallemailtemplates] = React.useState([]);

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };

    React.useEffect(() => {
        axios.get(`${API_SERVICE}/api/v1/main/findallemailtemplate/${uid}`)
            .then(response => {
                setallemailtemplates(response.data);
            })
    }, []);

    const showEmailTemplateAll = () => {
        return allemailtemplates.map(allemailtemplate => {
            return <EmailTemplateList allemailtemplate={allemailtemplate} key={allemailtemplate._id} />
        })
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="sm"
            >
                <DialogTitle style={{ color: 'black' }} id="alert-dialog-title">Create an email</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: 'black' }} id="alert-dialog-description">
                Keep your subscribers engaged by sharing your latest news, promoting a line of products, or announcing an event.
                </DialogContentText>
                    <div class="form-floating mb-3">
                        <input style={{ color: 'black' }} class="form-control" type="text" id="fl-text" placeholder="Campaign Name" />
                        <label for="fl-text">Campaign Name</label>
                    </div>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                <Button onClick={() => window.location.href = "/email"} color="primary" autoFocus>
                    Submit
                </Button>
                </DialogActions>
            </Dialog>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">Email Campaign</h1>
                            <a href="#!" onClick={handleClickOpen} className="btn btn-success float-right">Create New Email Campaign</a>
                            </div>
                            <div>
                                {showEmailTemplateAll()}
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default AllEmail