import React from 'react';
import { auth } from "../Firebase/index";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const SignIn = () => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const login = () => {
        auth.signInWithEmailAndPassword(email, password)
        .then(() => {
            auth.onAuthStateChanged(function(user) {
                if (user) {
                    sessionStorage.setItem("userId", user.uid);
                    sessionStorage.setItem("userEmail", user.email);
                    sessionStorage.setItem("login", true);
                    window.location.href = "/marketing-tool-dashboardhome";
                } else {
                    console.log("No User");
                }
            });
        })
        .catch(function(error) {
            var errorCode = error.code;
            if ( errorCode === "auth/wrong-password" ) {
                setEmail('');
                setPassword('');
                handleClickOpen();
                setMessage("Wrong Password");
            } else if ( errorCode === "auth/user-not-found" ) {
                setEmail('');
                setPassword('');
                handleClickOpen();
                setMessage("No User Found. Please Register Your Business. With Neighborhood Deals for Business");
            }
        });
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="sm"
            >
                <DialogTitle id="alert-dialog-title">Alert</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000' }} id="alert-dialog-description">
                    <center>
                        <img src="https://cdn.dribbble.com/users/2906251/screenshots/7835306/illustration_opt_4x.gif?compress=1&resize=400x300" className="w-75 img omg-fluid" />
                        <h4>
                            {message}
                        </h4>
                    </center>
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Okay
                </Button>
                </DialogActions>
            </Dialog>


            <main className="page-wrapper d-flex flex-column">
                <div className="modal fade" id="modal-signin" tabindex="-1">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content border-0">
                        <div className="view show" id="modal-signin-view">
                        <div className="modal-header border-0 bg-dark px-4">
                            <h4 className="modal-title text-light">Sign in</h4>
                            <button className="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close "></button>
                        </div>
                        <div className="modal-body px-4">
                            <p className="fs-ms text-muted">Sign in to your account using email and password provided during registration.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="mb-3">
                                <div className="input-group"><i className="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <input className="form-control rounded" value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Email" required="" />
                                </div>
                            </div>
                            <div className="mb-3">
                                <div className="input-group"><i className="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <div className="password-toggle w-100">
                                    <input className="form-control" value={password} onChange={(e) => setPassword(e.target.value)}  type="password" placeholder="Password" required="" />
                                    <label className="password-toggle-btn" aria-label="Show/hide password">
                                    </label>
                                </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between align-items-center mb-3 mb-3">
                                <div className="form-check">
                                <input className="form-check-input" type="checkbox" id="keep-signed" />
                                <label className="form-check-label fs-sm" for="keep-signed">Keep me signed in</label>
                                </div><a className="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                            </div>
                            <button onClick={login} className="btn btn-primary d-block w-100" type="button">Sign in</button>
                            <p className="fs-sm pt-3 mb-0">Register Your Business? <a href='/signup' className='fw-medium' data-view='#modal-signup-view'>Sign up</a></p>
                            </form>
                        </div>
                        </div>
                        <div className="view" id="modal-signup-view">
                        <div className="modal-header border-0 bg-dark px-4">
                            <h4 className="modal-title text-light">Sign up</h4>
                            <button className="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
                        </div>
                        <div className="modal-body px-4">
                            <p className="fs-ms text-muted">Registration takes less than a minute but gives you full control over your orders.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Full name" required="" />
                            </div>
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Email" required="" />
                            </div>
                            <div className="mb-3 password-toggle">
                                <input className="form-control" type="password" placeholder="Password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <div className="mb-3 password-toggle">
                                <input className="form-control" type="password" placeholder="Confirm password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <button className="btn btn-primary d-block w-100" type="submit">Sign up</button>
                            <p className="fs-sm pt-3 mb-0">Already have an account? <a href='#' className='fw-medium' data-view='#modal-signin-view'>Sign in</a></p>
                            </form>
                        </div>
                        </div>
                        <div className="modal-body text-center px-4 pt-2 pb-4">
                        <hr className="my-0" />
                        </div>
                    </div>
                    </div>
                </div>
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4" href="/">My Biz Promo</a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div className="d-none d-md-block position-absolute w-50 h-100 bg-size-cover" style={{ top: '0', right: '0', backgroundImage: "url(img/account/signin-img.jpg)" }}></div>
                <section className="container d-flex align-items-center pt-7 pb-3 pb-md-4" style={{ flex: '1 0 auto' }}>
                    <div className="w-100 pt-3">
                    <div className="row">
                        <div className="col-lg-4 col-md-6 offset-lg-1">
                        <div className="view show" id="signin-view">
                            <h1 className="h2">Sign in</h1>
                            <p className="fs-ms text-muted mb-4">Sign in to your account using email and password provided during registration.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="input-group mb-3"><i className="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <input value={email} onChange={(e) => setEmail(e.target.value)} className="form-control rounded" type="email" placeholder="Email" required="" />
                            </div>
                            <div className="input-group mb-3"><i className="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <div className="password-toggle w-100">
                                <input value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" type="password" placeholder="Password" required="" />
                                </div>
                            </div>
                            <div className="d-flex justify-content-between align-items-center mb-3 pb-1">
                                <div className="form-check">
                                <input className="form-check-input" type="checkbox" id="keep-signed-2" />
                                <label className="form-check-label" for="keep-signed-2">Keep me signed in</label>
                                </div><a className="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                            </div>
                            <button onClick={login} className="btn btn-primary d-block w-100" type="button">Sign in</button>
                            <p className="fs-sm pt-3 mb-0">Register Your Business <a href='/signup' className='fw-medium'>Register</a></p>
                            </form>
                        </div>
                        <div className="view" id="signup-view">
                            <h1 className="h2">Sign up</h1>
                            <p className="fs-ms text-muted mb-4">Registration takes less than a minute but gives you full control over your orders.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Full name" required="" />
                            </div>
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Email" required="" />
                            </div>
                            <div className="input-group mb-3">
                                <div className="password-toggle w-100">
                                <input className="form-control" type="password" placeholder="Password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                    <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <div className="password-toggle w-100">
                                <input className="form-control" type="password" placeholder="Confirm password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                    <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                                </div>
                            </div>
                            <button className="btn btn-primary d-block w-100" type="submit">Sign up</button>
                            <p className="fs-sm pt-3 mb-0">Already have an account? <a href='#' className='fw-medium' data-view='#signin-view'>Sign in</a></p>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                </section>
                </main>
        </>
    )
}

export default SignIn
