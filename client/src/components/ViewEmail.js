import React from 'react';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Container from '@material-ui/core/Container';
import parse from 'html-react-parser';
import CircularProgress from '@material-ui/core/CircularProgress';

const ViewEmail = ({ location }) => {
    const [campaign, setcampaign] = React.useState({});
    const [campaignId, setcampaignId] = React.useState('');
    const [loading, setloading] = React.useState(true);

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchaemailcampign/${q}`)
            .then((d) => {
                setcampaign(d.data[0]);
                setloading(false);
            }).catch(err => console.log(err));
        setcampaignId(q);
    }, []);

    return (
        <>
            <Container maxWidth="sm">
                {
                    loading ? (
                        <center style={{ marginTop: '50%' }}>
                            <h1> <CircularProgress /> Loading Email</h1>
                        </center>
                    ) : (
                        <>
                        {parse(`
                            ${campaign.designcontent}
                        `)}
                        </>
                    )
                }
                
            </Container>
        </>
    )
}

export default ViewEmail
