import React from 'react';
import Profile from './static/Profile';

const Ads = () => {

    const uid = sessionStorage.getItem("userId");
   
    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">Google & Facebook Ads</h1>
                            <button onClick={() => window.open(`/posters`, '_blank', 'location=yes,height=900,width=1200,scrollbars=yes,status=yes')} className="btn btn-success">
                                Ads Desgning
                            </button>
                            </div>
                            <div class="row no-gutters mx-n2 mb-4">
                                <div className="card mt-2">
                                    <div className="card-body">
                                        <img src="https://img.icons8.com/fluent/48/000000/google-logo.png"/>
                                        <h5 className="card-title">Google Ads</h5>
                                        <p className="card-text fs-sm">
                                        Google Ads is an online advertising platform developed by Google, where advertisers bid to display brief advertisements, service offerings, product listings, or videos to web users. It can place ads both in the results of search engines like Google Search and on non-search websites, mobile apps, and videos.
                                        </p>
                                        <a target="_blank" href="https://ads.google.com/aw/campaigns?ocid=294540221&euid=305486869&__u=5231396781&uscid=294540221&__c=1034052629&authuser=0" className="btn btn-sm btn-primary">Visit Now</a>
                                    </div>
                                </div>

                                <div className="card mt-2">
                                    <div className="card-body">
                                        <img src="https://img.icons8.com/fluent/48/000000/facebook-new.png"/>
                                        <h5 className="card-title">Facebook Ads</h5>
                                        <p className="card-text fs-sm">
                                        If you're an advertiser on Facebook who shares access to multiple Pages and ad accounts with other people, we recommend you transition to Business
                                        </p>
                                        <a target="_blank" href="https://www.facebook.com/business/ads" className="btn btn-sm btn-primary">Visit Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="d-sm-flex align-items-center text-center text-sm-start">
                            <h6 class="text-nowrap my-2 me-3">Share this list:</h6><a class="btn-social bs-facebook me-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter me-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-email me-2 my-2" href="#"><i class="ai-mail"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default Ads