import React from 'react'

const Signup = () => {
    return (
        <>
            <main className="page-wrapper">
                <div className="modal fade" id="modal-signin" tabindex="-1">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content border-0">
                        <div className="view show" id="modal-signin-view">
                        <div className="modal-header border-0 bg-dark px-4">
                            <h4 className="modal-title text-light">Sign in</h4>
                            <button className="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close "></button>
                        </div>
                        <div className="modal-body px-4">
                            <p className="fs-ms text-muted">Sign in to your account using email and password provided during registration.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="mb-3">
                                <div className="input-group"><i className="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <input className="form-control rounded" type="email" placeholder="Email" required="" />
                                </div>
                            </div>
                            <div className="mb-3">
                                <div className="input-group"><i className="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                <div className="password-toggle w-100">
                                    <input className="form-control" type="password" placeholder="Password" required="" />
                                    <label className="password-toggle-btn" aria-label="Show/hide password">
                                    <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                    </label>
                                </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between align-items-center mb-3 mb-3">
                                <div className="form-check">
                                <input className="form-check-input" type="checkbox" id="keep-signed" />
                                <label className="form-check-label fs-sm" for="keep-signed">Keep me signed in</label>
                                </div><a className="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                            </div>
                            <button className="btn btn-primary d-block w-100" type="submit">Sign in</button>
                            <p className="fs-sm pt-3 mb-0">Don't have an account? <a href='#' className='fw-medium' data-view='#modal-signup-view'>Sign up</a></p>
                            </form>
                        </div>
                        </div>
                        <div className="view" id="modal-signup-view">
                        <div className="modal-header border-0 bg-dark px-4">
                            <h4 className="modal-title text-light">Sign up</h4>
                            <button className="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
                        </div>
                        <div className="modal-body px-4">
                            <p className="fs-ms text-muted">Registration takes less than a minute but gives you full control over your orders.</p>
                            <form className="needs-validation" novalidate="">
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Full name" required="" />
                            </div>
                            <div className="mb-3">
                                <input className="form-control" type="text" placeholder="Email" required="" />
                            </div>
                            <div className="mb-3 password-toggle">
                                <input className="form-control" type="password" placeholder="Password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <div className="mb-3 password-toggle">
                                <input className="form-control" type="password" placeholder="Confirm password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <button className="btn btn-primary d-block w-100" type="submit">Sign up</button>
                            <p className="fs-sm pt-3 mb-0">Already have an account? <a href='#' className='fw-medium' data-view='#modal-signin-view'>Sign in</a></p>
                            </form>
                        </div>
                        </div>
                        <div className="modal-body text-center px-4 pt-2 pb-4">
                        <hr className="my-0" />
                        <p className="fs-sm fw-medium text-heading pt-4">Or sign in with</p><a className="btn-social bs-facebook bs-lg mx-1 mb-2" href="#"><i className="ai-facebook"></i></a><a className="btn-social bs-twitter bs-lg mx-1 mb-2" href="#"><i className="ai-twitter"></i></a><a className="btn-social bs-instagram bs-lg mx-1 mb-2" href="#"><i className="ai-instagram"></i></a><a className="btn-social bs-google bs-lg mx-1 mb-2" href="#"><i className="ai-google"></i></a>
                        </div>
                    </div>
                    </div>
                </div>
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4" href="/">Company Name</a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div className="container py-5 py-md-7">
                    <div className="row align-items-center pt-2">
                    <div className="col-md-6 col-lg-5 mb-5 mb-md-0">
                        <div className="bg-secondary px-4 py-5 p-sm-5 rounded-3">
                        <h1 className="h3 pt-1">Sign in</h1>
                        <p className="fs-ms text-muted">Sign in to your account using email and password provided during registration.</p>
                        <form className="needs-validation" novalidate="">
                            <div className="input-group mb-3"><i className="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                            <input className="form-control rounded" type="email" placeholder="Email" required="" />
                            </div>
                            <div className="input-group mb-3"><i className="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                            <div className="password-toggle w-100">
                                <input className="form-control" type="password" placeholder="Password" required="" />
                                <label className="password-toggle-btn" aria-label="Show/hide password">
                                <input className="password-toggle-check" type="checkbox" /><span className="password-toggle-indicator"></span>
                                </label>
                            </div>
                            </div>
                            <div className="d-flex justify-content-between align-items-center mb-3 pb-1">
                            <div className="form-check">
                                <input className="form-check-input" type="checkbox" id="keep-signed-2" />
                                <label className="form-check-label fs-sm" for="keep-signed-2">Keep me signed in</label>
                            </div><a className="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                            </div>
                            <button className="btn btn-primary d-block w-100" type="submit">Sign in</button>
                            <div className="border-top text-center mt-4 pt-4">
                            <p className="fs-sm fw-medium text-heading">Or sign in with</p><a className="btn-social bs-facebook bs-outline bs-lg mx-1 mb-2" href="#"><i className="ai-facebook"></i></a><a className="btn-social bs-twitter bs-outline bs-lg mx-1 mb-2" href="#"><i className="ai-twitter"></i></a><a className="btn-social bs-instagram bs-outline bs-lg mx-1 mb-2" href="#"><i className="ai-instagram"></i></a><a className="btn-social bs-google bs-outline bs-lg mx-1 mb-2" href="#"><i className="ai-google"></i></a>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div className="col-md-6 offset-lg-1">
                        <h2 className="h3">No account? Sign up</h2>
                        <p className="fs-ms text-muted">Registration takes less than a minute but gives you full control over your orders.</p>
                        <form className="row needs-validation" novalidate="">
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-fn">First name<sup className="text-danger ms-1">*</sup></label>
                            <input className="form-control" type="text" required="" id="reg-fn" />
                            <div className="invalid-feedback">Please enter you first name!</div>
                        </div>
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-ln">Last name<sup className="text-danger ms-1">*</sup></label>
                            <input className="form-control" type="text" required="" id="reg-ln" />
                            <div className="invalid-feedback">Please enter you last name!</div>
                        </div>
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-email">Email address<sup className="text-danger ms-1">*</sup></label>
                            <input className="form-control" type="email" required="" id="reg-email" />
                            <div className="invalid-feedback">Please enter a valid email address!</div>
                        </div>
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-phone">Phone number</label>
                            <input className="form-control bg-image-0" type="text" id="reg-phone" />
                        </div>
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-password">Password<sup className="text-danger ms-1">*</sup></label>
                            <input className="form-control" type="password" required="" id="reg-password" />
                            <div className="invalid-feedback">Please provide password!</div>
                        </div>
                        <div className="col-sm-6 mb-3">
                            <label className="form-label" for="reg-confirm-password">Confirm password<sup className="text-danger ms-1">*</sup></label>
                            <input className="form-control" type="password" required="" id="reg-confirm-password" />
                            <div className="invalid-feedback">Password doesn't match!</div>
                        </div>
                        <div className="col-sm-6 pt-2">
                            <button className="btn btn-primary d-block w-100" type="submit">Sign up</button>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default Signup
