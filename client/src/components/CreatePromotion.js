import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Profile from './static/Profile';

const useStyles = makeStyles((theme) => ({
    appBar: {
      position: 'relative',
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 500,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
  
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const CreatePromotion = () => {
    const classes = useStyles();
    const userId = sessionStorage.getItem("userId");


    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    }

    const [organization, setorganization] = React.useState('');
    const [organizationName, setorganizationName] = React.useState('');
    const handleChangeOrganization = (event) => {
        setorganization(event.target.value);
    };

    const [templateversion, settemplateversion] = React.useState('');
    const handleChangeTemplateversion = (event) => {
        settemplateversion(event.target.value);
    };

    const [openCreateOrganization, setOpenCreateOrganization] = React.useState(false);
    const handleClickOpenCreateOrganization = () => {
        setOpenCreateOrganization(true);
    };
    const handleCloseCreateOrganization = () => {
        setOpenCreateOrganization(false);
    };

    const createPromotion = () => {
        var uploadData = {
            organization: 'Limo',
            templateversion,
            userId
        }
        axios.post(`${API_SERVICE}/api/v1/main/createpromotion`, uploadData)
            .then((res) => {
                window.location.href = `/marketing-tool-designpromotion?q=${res.data._id}`;
            }).catch(err => console.log(err));
    }

    const createNewOrganization = () => {
        var uploadData = {
            organizationName,
            userId
        }
        axios.post(`${API_SERVICE}/api/v1/main/createneworganization`, uploadData)
            .then((res) => {
                setorganization(organizationName);
                handleCloseCreateOrganization();
            }).catch(err => console.log(err));
    }

    return (
        <>
            <Dialog
                open={openCreateOrganization}
                onClose={handleCloseCreateOrganization}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="sm"
            >
                <DialogTitle id="alert-dialog-title">{"Name of your Organization"}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <br />
                    <TextField onChange={(e) => setorganizationName(e.target.value)} id="standard-basic" label="Organization Name" fullWidth />
                    <br />
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={createNewOrganization} color="primary">
                    Submit
                </Button>
                <Button onClick={handleCloseCreateOrganization} color="primary" autoFocus>
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                    <CloseIcon />
                    </IconButton>
                </Toolbar>
                </AppBar>
                <div className="container">
                    <center>
                        <h4 className="font-weight-bold display-4 mt-4 text-dark mb-5">
                            Create Promotions
                        </h4>
                        <br />
                        <FormControl className={classes.formControl}>
                            <InputLabel id="demo-simple-select-outlined-label">Template Version</InputLabel>
                            <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={templateversion}
                            onChange={handleChangeTemplateversion}
                            fullWidth
                            >
                            <MenuItem value='White Label'>White Label (The most customizable for large brands)</MenuItem>
                            <MenuItem value='Premimum'>Premimum (The most popular for professionals)</MenuItem>
                            </Select>
                        </FormControl>
                        <br />
                        <br />
                        <Button onClick={createPromotion} variant="contained" size="large" color="primary">
                            Create Promotion
                        </Button>
                    </center>

                </div>
            </Dialog>

            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            </div>
                            <div class="row no-gutters mx-n2 mb-4">
                            <div class="col-md-4 col-sm-6 px-2 mb-3">
                                <div class="card card-product card-hover"><center><img alt="Coupons" src="https://img.icons8.com/fluent/96/000000/redeem.png"/></center>
                                <div class="card-body"><h2>Coupons</h2>
                                </div>
                                <center className="mb-4 p-2">
                                    <button onClick={handleClickOpen} style={{ width: '100%' }} className="btn btn-success">
                                        Select
                                    </button>
                                </center>
                                </div>
                            </div>
                            </div>
                            <div class="d-sm-flex align-items-center text-center text-sm-start">
                            <h6 class="text-nowrap my-2 me-3">Share this list:</h6><a class="btn-social bs-facebook me-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter me-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-email me-2 my-2" href="#"><i class="ai-mail"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default CreatePromotion
