import React from 'react';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

const Message = () => {
    const uid = "XYZ00";

    const [message, setmessage] = React.useState('');
    const [phonenumber, setphonenumber] = React.useState('');

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const sendMessage = () => {
        var uploadData = {
            phonenumber,
            message
        }
        axios.post(`${API_SERVICE}/api/v1/main/sendsmsphonenumber`, uploadData)
            .then(() => {
                handleClickOpen();
                setphonenumber("");
                setmessage("");
            }).catch(err => console.log(err));
    }
   
    return (
        <>
            <Dialog
                open={open}
                fullWidth={true}
                maxWidth="sm"
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">SMS Send Successfully</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000' }} id="alert-dialog-description">
                    Your SMS to Phone Number {phonenumber} was successfully Send.
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Okay
                </Button>
                </DialogActions>
            </Dialog>

            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <center className="mt-2 mb-2">
                        <div className="h2" id="google_translate_element"></div>
                    </center>
                    <div class="form-floating mb-3">
                        <input value={phonenumber} onChange={(e) => setphonenumber(e.target.value)} class="form-control" type="text" id="fl-text" placeholder="+1XXXXXXXXXX" />
                        <label for="fl-text">Phone Number E.164 Format (+1XXXXXXXXXX)</label>
                        </div>
                    </div>

                    <div class="form-floating">
                        <textarea value={message} onChange={(e) => setmessage(e.target.value)} class="form-control" id="fl-textarea" style={{ height: '120px' }} placeholder="Your message"></textarea>
                        <label for="fl-textarea">Your message</label>
                    </div>
                    <button onClick={sendMessage} className="btn btn-success w-100 mt-2 btn-lg">Send</button>
                    <br />
                    <br />
                    <br />
                    <br />
                    <h1 className="font-weight-bold mt-5">Templates</h1>
                    <h4 className="font-weight-bold mt-2">Emergency SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">COVID 19</h5>
                                    <p class="card-text">
                                    When you need to share important information quickly, business text templates for emergency situations can save valuable time. Share vital information with customers and employees about fast-moving situations such as the COVID-19 outbreak to mobilize your teams and keep everyone informed.  
                                    </p>
                                    <button onClick={(e) => setmessage("When you need to share important information quickly, business text templates for emergency situations can save valuable time. Share vital information with customers and employees about fast-moving situations such as the COVID-19 outbreak to mobilize your teams and keep everyone informed.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Health Announcement</h5>
                                    <p class="card-text">
                                    As the HEALTH ISSUE impacts our region, we’re working to ensure our customers’ needs continue to be met while we do our part to help mitigate this serious issue. We will continue to provide additional guidance as new information becomes available.   
                                    </p>
                                    <button onClick={(e) => setmessage("As the HEALTH ISSUE impacts our region, we’re working to ensure our customers’ needs continue to be met while we do our part to help mitigate this serious issue. We will continue to provide additional guidance as new information becomes available.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Doctor Waiting Room</h5>
                                    <p class="card-text">
                                    We look forward to seeing you for your appointment today. For your safety, we ask that you check in from your car when you arrive by responding with the word HERE to this text. You will then receive a text message when the doctor is ready to see you. Thank you for your cooperation.
                                    </p>
                                    <button onClick={(e) => setmessage("We look forward to seeing you for your appointment today. For your safety, we ask that you check in from your car when you arrive by responding with the word HERE to this text. You will then receive a text message when the doctor is ready to see you. Thank you for your cooperation.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Severe Weather Warning</h5>
                                    <p class="card-text">
                                    Due to severe weather in the area, local authorities are recommending that everyone remain indoors for the next TIMEFRAME. More updates will be shared as new information becomes available. 
                                    </p>
                                    <button onClick={(e) => setmessage("Due to severe weather in the area, local authorities are recommending that everyone remain indoors for the next TIMEFRAME. More updates will be shared as new information becomes available.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Transit Schedule Change</h5>
                                    <p class="card-text">
                                    Bus routes #XXX, #XXX, #XXX will be rerouted until further notice due to REASON. We will send an update when the routes return to their regular schedule. Get details here: WEB LINK. 
                                    </p>
                                    <button onClick={(e) => setmessage("Bus routes #XXX, #XXX, #XXX will be rerouted until further notice due to REASON. We will send an update when the routes return to their regular schedule. Get details here: WEB LINK. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">School Closure</h5>
                                    <p class="card-text">
                                    Due to REASON, the SCHOOL DISTRICT will be closed on DATE. We will keep you updated about possible future closures. Click this link for details: INSERT LINK. 
                                    </p>
                                    <button onClick={(e) => setmessage("Due to REASON, the SCHOOL DISTRICT will be closed on DATE. We will keep you updated about possible future closures. Click this link for details: INSERT LINK.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <h4 className="font-weight-bold mt-2">Confirmation SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Appointment</h5>
                                    <p class="card-text">
                                    Thank you for scheduling an appointment with our office. You are confirmed for an appointment with INDIVIDUAL on DATE at TIME. If you have any questions, please respond to this text or call HIM/HER at NUMBER. 
                                    </p>
                                    <button onClick={(e) => setmessage("Thank you for scheduling an appointment with our office. You are confirmed for an appointment with INDIVIDUAL on DATE at TIME. If you have any questions, please respond to this text or call HIM/HER at NUMBER.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Package Delivery</h5>
                                    <p class="card-text">
                                    Your package has been delivered to the address we have on file. If you have any questions, please let us know. 
                                    </p>
                                    <button onClick={(e) => setmessage("Your package has been delivered to the address we have on file. If you have any questions, please let us know. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Information Received</h5>
                                    <p class="card-text">
                                    Thank you for providing the information we requested. We will follow up with you shortly regarding the next steps. 
                                    </p>
                                    <button onClick={(e) => setmessage("Thank you for providing the information we requested. We will follow up with you shortly regarding the next steps. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <h4 className="font-weight-bold mt-2">Reminder SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Appointment</h5>
                                    <p class="card-text">
                                    This is a friendly reminder that you have an appointment with INDIVIDUAL on DATE at TIME for [Custom Field 1]. If you have any questions, you can reply via text or call. 
                                    </p>
                                    <button onClick={(e) => setmessage("This is a friendly reminder that you have an appointment with INDIVIDUAL on DATE at TIME for [Custom Field 1]. If you have any questions, you can reply via text or call.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Document Request</h5>
                                    <p class="card-text">
                                    We just wanted to remind you that we’re waiting for the DOCUMENT you agreed to send us so we can complete the TRANSACTION we discussed. If you have any questions, please text or phone us. 
                                    </p>
                                    <button onClick={(e) => setmessage("We just wanted to remind you that we’re waiting for the DOCUMENT you agreed to send us so we can complete the TRANSACTION we discussed. If you have any questions, please text or phone us.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Past Due Bill</h5>
                                    <p class="card-text">
                                    Dear [First Name], our records indicate that your account has a balance that is past due. Please visit this link to our online payment page: WEB LINK by DATE in order to avoid a late fee. Text us if you have questions. 
                                    </p>
                                    <button onClick={(e) => setmessage("Dear [First Name], our records indicate that your account has a balance that is past due. Please visit this link to our online payment page: WEB LINK by DATE in order to avoid a late fee. Text us if you have questions. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <h4 className="font-weight-bold mt-2">Marketing & Promotion SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Giveaway</h5>
                                    <p class="card-text">
                                    It’s [Custom Field 1]’s BOGO Event! Sale starts DATE and ends DATE. Hurry in to take advantage of this limited-time offer. To find a store near you, visit: URL. 
                                    </p>
                                    <button onClick={(e) => setmessage("It’s [Custom Field 1]’s BOGO Event! Sale starts DATE and ends DATE. Hurry in to take advantage of this limited-time offer. To find a store near you, visit: URL.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Seasonal Sale</h5>
                                    <p class="card-text">
                                    Hi, [First Name]! Don’t miss our SEASON sale on ITEM, ITEM and more! Save up to XX% on everything you’ll need for the season. For more info visit: URL. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi, [First Name]! Don’t miss our SEASON sale on ITEM, ITEM and more! Save up to XX% on everything you’ll need for the season. For more info visit: URL.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Open House</h5>
                                    <p class="card-text">
                                    We’re super excited to announce that we’re opening our doors on DATE at TIME. Come join us! For more details visit:S WEBITE LINK. We look forward to seeing you there! 
                                    </p>
                                    <button onClick={(e) => setmessage("We’re super excited to announce that we’re opening our doors on DATE at TIME. Come join us! For more details visit:S WEBITE LINK. We look forward to seeing you there! ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <h4 className="font-weight-bold mt-2">Sales SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Introductions</h5>
                                    <p class="card-text">
                                    Hi, [First Name). This is [My First Name] from [Custom Field 1]. Are you available for a short call to discuss [Custom Field 2]? Please let me know if you’re interested by replying to this message. I look forward to speaking with you. Thank you. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi, [First Name). This is [My First Name] from [Custom Field 1]. Are you available for a short call to discuss [Custom Field 2]? Please let me know if you’re interested by replying to this message. I look forward to speaking with you. Thank you. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Relationship Building</h5>
                                    <p class="card-text">
                                    Hi, [First Name). This is [My First Name] from [Custom Field 1]. I just wanted to check in to see if you have any questions about [Custom Field 1]. I’m available for a quick call or text conversation if you’d like. You can text or call me at this number: [My Phone Number]. Have a great day! 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi, [First Name). This is [My First Name] from [Custom Field 1]. I just wanted to check in to see if you have any questions about [Custom Field 1]. I’m available for a quick call or text conversation if you’d like. You can text or call me at this number: [My Phone Number]. Have a great day! ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Sales Follow-up</h5>
                                    <p class="card-text">
                                    Hi, [First Name). This is [My First Name] from [Custom Field 1]. I just wanted to follow up on [Custom Field 1]. Feel free to text or call [My Phone Number] at your convenience. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi, [First Name). This is [My First Name] from [Custom Field 1]. I just wanted to follow up on [Custom Field 1]. Feel free to text or call [My Phone Number] at your convenience. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <hr />
                    <h4 className="font-weight-bold mt-2">Internal SMS</h4>
                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Volunteer Opportunity</h5>
                                    <p class="card-text">
                                    Hey team! Who’s interested in joining us for our FREQUENCY volunteer event? We’ll be helping ORGANIZATION on DATE between TIME and TIME. RSVP if you can join us!  
                                    </p>
                                    <button onClick={(e) => setmessage("Hey team! Who’s interested in joining us for our FREQUENCY volunteer event? We’ll be helping ORGANIZATION on DATE between TIME and TIME. RSVP if you can join us!")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Online Review</h5>
                                    <p class="card-text">
                                    Hi [First Name], this is INDIVIDUAL from COMPANY. Thank you for being a valued customer. We would appreciate it if you could leave a review about your experience with us. We have provided a link to a third-party online review page for your convenience: ADD URL. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi [First Name], this is INDIVIDUAL from COMPANY. Thank you for being a valued customer. We would appreciate it if you could leave a review about your experience with us. We have provided a link to a third-party online review page for your convenience: ADD URL.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Oil Change Reminder</h5>
                                    <p class="card-text">
                                    Hello, [First Name].This is a reminder that your YEAR VEHICLE will be due for an oil change soon. Respond to this message or phone [My Phone Number] to schedule an appointment. INDIVIDUAL at BUSINESS. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hello, [First Name].This is a reminder that your YEAR VEHICLE will be due for an oil change soon. Respond to this message or phone [My Phone Number] to schedule an appointment. INDIVIDUAL at BUSINESS.")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-2">
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Service Interval Appointment</h5>
                                    <p class="card-text">
                                    Hello, [First Name]. Our records indicate that your YEAR VEHICLE is due for its NUMBER-mile tune-up. To schedule an appointment, reply by texting us or calling MY NUMBER. INDIVIDUAL at BUSINESS. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hello, [First Name]. Our records indicate that your YEAR VEHICLE is due for its NUMBER-mile tune-up. To schedule an appointment, reply by texting us or calling MY NUMBER. INDIVIDUAL at BUSINESS. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Vehicle Ready for Pick-up</h5>
                                    <p class="card-text">
                                    Hello, [First Name]. Your YEAR VEHICLE is ready for pick up. We’re open from TIME until TIME today. We look forward to seeing you soon. If you have any questions, please respond to this text message or phone MY NUMBER. 
                                    </p>
                                    <button onClick={(e) => setmessage("Hello, [First Name]. Your YEAR VEHICLE is ready for pick up. We’re open from TIME until TIME today. We look forward to seeing you soon. If you have any questions, please respond to this text message or phone MY NUMBER. ")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div class="card border-0 shadow">
                                <div class="card-body">
                                    <h5 class="card-title">Class or Appointment Reminder</h5>
                                    <p class="card-text">
                                    Hi, CUSTOMER! Don’t forget: you’re registered for CLASS at TIME today. If you need to cancel, reply to this message. Otherwise, we’ll see you there! 
                                    </p>
                                    <button onClick={(e) => setmessage("Hi, CUSTOMER! Don’t forget: you’re registered for CLASS at TIME today. If you need to cancel, reply to this message. Otherwise, we’ll see you there!")} class="btn btn-sm btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    

                </div>
                </main>
        </>
    )
}

export default Message