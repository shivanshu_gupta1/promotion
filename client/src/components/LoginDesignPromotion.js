import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
}));

const LoginDesignPromotion = () => {
    const classes = useStyles();
    const [statelogin, setStatelogin] = React.useState({
        Google: false,
        Facebook: false,
        Email: true,
      });
    
    const handleChange = (event) => {
        setStatelogin({ ...state, [event.target.name]: event.target.checked });
    };

    const [state, setState] = React.useState({
        left: false
      });
    
    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
          <List>
            <ListItem onClick={() => window.location.href = "/designpromotion"} button href="/designpromotion">
                <ListItemText primary='Create/Design Template' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/logindesignpromotion"} button href="/logindesignpromotion">
                <ListItemText primary='Login' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/registrationdesignpromotion"} button href="/registerdesignpromotion">
                <ListItemText primary='Registration Form' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/thankyoudesignpromotion"} button href="/participationdesignpromotion">
                <ListItemText primary='Thank You' />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem onClick={() => window.location.href = "/datesdesignpromotion"} button href="/designpromotion">
                <ListItemText primary='Dates' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/languagesdesignpromotion"} button href="/logindesignpromotion">
                <ListItemText primary='Languages' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/prizedesignpromotion"} button href="/registerdesignpromotion">
                <ListItemText primary='Prize' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/leagaldesignpromotion"} button href="/participationdesignpromotion">
                <ListItemText primary='Leagal Content' />
            </ListItem>
            <ListItem onClick={() => window.location.href = "/sharedesignpromotion"} button href="/participationdesignpromotion">
                <ListItemText primary='Share' />
            </ListItem>
          </List>
        </div>
    );

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <div className={classes.root}>
                    
                    <Grid container spacing={3}>
                            <Grid item xs={4}>
                                {['left'].map((anchor) => (
                                <React.Fragment key={anchor}>
                                <Button startIcon={<MoreHorizIcon />} fullWidth style={{ backgroundColor: '#fff', color: '#000', marginBottom: '4px' }} variant="contained" onClick={toggleDrawer(anchor, true)}>More Options</Button>
                                <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                                    {list(anchor)}
                                </Drawer>
                                </React.Fragment>
                            ))}
                            <Paper className={classes.paper}>
                                <Button startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                    Save
                                </Button>
                                <br />
                                <FormControlLabel
                                    control={<Checkbox checked={statelogin.Email} onChange={handleChange} name="Email" />}
                                    label="Email"
                                />
                                <br />
                                <FormControlLabel
                                    control={<Checkbox checked={statelogin.Google} onChange={handleChange} name="Google" />}
                                    label="Google"
                                />
                                <br />
                                <FormControlLabel
                                    control={<Checkbox checked={statelogin.Facebook} onChange={handleChange} name="Facebook" />}
                                    label="Facebook"
                                />
                            </Paper>    
                        </Grid>
                        <Grid className="stick" item xs={8}>
                            <Paper className={classes.paper}>
                                <section className="borderlogin">
                                    <center>
                                        <img className="w-25" src="http://assets.stickpng.com/images/5aaa4b727603fc558cffbf9b.png" />
                                    </center>
                                    <br />
                                    <h4 style={{ float: 'left' }}>Email</h4>
                                    <TextField fullWidth id="outlined-basic" type="email" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Password</h4>
                                    <TextField fullWidth id="outlined-basic" type="password" variant="outlined" />
                                    <br />
                                    <br />
                                    <Button fullWidth size="large" variant="contained" color="primary">
                                        Login
                                    </Button>
                                    <br />
                                    
                                    {
                                        statelogin.Google ? (
                                            <>
                                            <center style={{ marginTop: '10px' }}>
                                                OR
                                            </center>
                                            <Button style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                                                <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
                                                Continue with Google
                                            </Button>
                                            <br />
                                            </>
                                        ) : null
                                    }

                                    {
                                        statelogin.Facebook ? (
                                            <Button style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                                                <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
                                                Continue with Facebook
                                            </Button>
                                        ) : null
                                    }
                                    
                                    

                                </section>
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
                </div>
                </main>
        </>
    )
}

export default LoginDesignPromotion
