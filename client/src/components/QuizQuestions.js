import React from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import parse from 'html-react-parser';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';

const minuteSeconds = 60;
const hourSeconds = 3600;
const daySeconds = 86400;

const timerProps = {
  isPlaying: true,
  size: 120,
  strokeWidth: 6
};

const renderTime = (dimension, time) => {
  return (
    <div className="time-wrapper">
      <div className="time">{time}</div>
      <div>{dimension}</div>
    </div>
  );
};

const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
const getTimeDays = (time) => (time / daySeconds) | 0;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
    },
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
}));

const QuizQuestions = ({ location }) => {
    const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = stratTime + 243248; // use UNIX timestamp in seconds
    const remainingTime = endTime - stratTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;

    const classes = useStyles();
    const userId = sessionStorage.getItem("userId");

    const [quizId, setquizId] = React.useState('');
    const [update, setupdate] = React.useState(false);
    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchquiz/${q}`)
            .then((d) => {
                settitle(d.data[0].title);
            }).catch(err => console.log(err));
        setquizId(q);
    }, []);


    const [title, settitle] = React.useState('✅ Enter your code and [TRY YOUR LUCK/ENTER THE PRIZE DRAW] 🍀');
    const [label, setlabel] = React.useState('Begin the quiz');
    const [description, setdescription] = React.useState(`
    <h3>🎯 We will raffle [THIS PRIZE] among [all participants that reach top score]</strong>.</h3>

    ✅ Follow these steps to participate:

    <ul><li>▶️ Answer all the <strong>quiz questions</strong>.</li>
    <li>▶️ <strong>Register</strong> in the entry form and you'll know if you've answered correctly.</li>
    <li>➡️ <strong>Share</strong> the quiz with your friends. </li>
    </ul>

    🗓️ Promotion valid until dd-mm-yyyy`);

    const [state, setState] = React.useState({
        left: false
      });
    
    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
          <List>
            <ListItem onClick={() => window.location.href = `/marketing-tool-quiz`} button href="/designpromotion">
                <ListItemText primary='All Quiz' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-createquiz?q=${quizId}`} button href="/designpromotion">
                <ListItemText primary='Create/Design Quiz' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-questions?q=${quizId}`} button >
                <ListItemText primary='Questions' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-registrationquiz?q=${quizId}`} button >
                <ListItemText primary='Registration Form' />
            </ListItem>
            <ListItem onClick={() => window.location.href = `/marketing-tool-sharequiz?q=${quizId}`} button >
                <ListItemText primary='Share Quiz' />
            </ListItem>
          </List>
        </div>
    );

    // Quiz
    const [question1, setquestion1] = React.useState('');
    const [option11, setoption11] = React.useState('');
    const [option12, setoption12] = React.useState('');
    const [option13, setoption13] = React.useState('');
    const [option14, setoption14] = React.useState('');
    const [answer1, setanswer1] = React.useState('');


    const [question2, setquestion2] = React.useState('');
    const [option21, setoption21] = React.useState('');
    const [option22, setoption22] = React.useState('');
    const [option23, setoption23] = React.useState('');
    const [option24, setoption24] = React.useState('');
    const [answer2, setanswer2] = React.useState('');

    const [question3, setquestion3] = React.useState('');
    const [option31, setoption31] = React.useState('');
    const [option32, setoption32] = React.useState('');
    const [option33, setoption33] = React.useState('');
    const [option34, setoption34] = React.useState('');
    const [answer3, setanswer3] = React.useState('');


    const [question4, setquestion4] = React.useState('');
    const [option41, setoption41] = React.useState('');
    const [option42, setoption42] = React.useState('');
    const [option43, setoption43] = React.useState('');
    const [option44, setoption44] = React.useState('');
    const [answer4, setanswer4] = React.useState('');

    const saveQuiz = (question, option1,option2, option3, option4, answers) => {
        var uploadData = {
            quizId,
            question,
            option1,
            option2,
            option3,
            option4,
            answers
        }
        axios.post(`${API_SERVICE}/api/v1/main/savequizquestion`, uploadData)
            .then(() => {
                setupdate(true);
            }).catch(err => console.log(err));
    }

    return (
        <>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                <div className={classes.root}>
                    {
                        update ? (
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Holy guacamole!</strong> Your Question is successfully saved
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ) : null
                    }
                    
                    <Grid container spacing={3}>
                            
                            <Grid item xs={4}>
                                {['left'].map((anchor) => (
                                <React.Fragment key={anchor}>
                                <Button startIcon={<MoreHorizIcon />} fullWidth style={{ backgroundColor: '#fff', color: '#000', marginBottom: '4px' }} variant="contained" onClick={toggleDrawer(anchor, true)}>More Options</Button>
                                <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                                    {list(anchor)}
                                </Drawer>
                                </React.Fragment>
                            ))}
                        </Grid>
                        <Grid className="stick" item xs={8}>
                            <Paper className={classes.paper}>
                                <section className="borderBanner2">
                                    <center>
                                        <img className="imagebanner" src="https://www.bragitoff.com/wp-content/uploads/2015/03/quiz.jpg" />
                                    </center>
                                    <div style={{ color: '#000' }} className="title">
                                        <h1 style={{ float: 'left !important' }}>
                                            Question 1
                                        </h1>
                                        <div className="form-floating mb-3">
                                            <TextField value={question1} onChange={(e) => setquestion1(e.target.value)} id="outlined-basic" placeholder="Question 1" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <TextField value={option11} onChange={(e) => setoption11(e.target.value)}  id="outlined-basic" placeholder="Option 1" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option12} onChange={(e) => setoption12(e.target.value)}  id="outlined-basic" placeholder="Option 2" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option13} onChange={(e) => setoption13(e.target.value)}  id="outlined-basic" placeholder="Option 3" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option14} onChange={(e) => setoption14(e.target.value)}  id="outlined-basic" placeholder="Option 4" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Correct Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <select value={answer1} onChange={(e) => setanswer1(e.target.value)}  class="form-select" id="fl-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <Button onClick={() => saveQuiz(question1, option11, option12, option13, option14, answer1)} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                            Save Question
                                        </Button>
                                    </div>
                                    <br />
                                    <hr />

                                    <div style={{ color: '#000' }} className="title">
                                        <h1 style={{ float: 'left !important' }}>
                                            Question 2
                                        </h1>
                                        <div className="form-floating mb-3">
                                            <TextField value={question2} onChange={(e) => setquestion2(e.target.value)} id="outlined-basic" placeholder="Question 2" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <TextField value={option21} onChange={(e) => setoption21(e.target.value)}  id="outlined-basic" placeholder="Option 1" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option22} onChange={(e) => setoption22(e.target.value)}  id="outlined-basic" placeholder="Option 2" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option23} onChange={(e) => setoption23(e.target.value)}  id="outlined-basic" placeholder="Option 3" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option24} onChange={(e) => setoption24(e.target.value)}  id="outlined-basic" placeholder="Option 4" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Correct Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <select value={answer2} onChange={(e) => setanswer2(e.target.value)}  class="form-select" id="fl-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <Button onClick={() => saveQuiz(question2, option21, option22, option23, option24, answer2)} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                            Save Question
                                        </Button>
                                    </div>
                                    <hr />
                                    <div style={{ color: '#000' }} className="title">
                                        <h1 style={{ float: 'left !important' }}>
                                            Question 3
                                        </h1>
                                        <div className="form-floating mb-3">
                                            <TextField value={question3} onChange={(e) => setquestion3(e.target.value)} id="outlined-basic" placeholder="Question 3" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <TextField value={option31} onChange={(e) => setoption31(e.target.value)}  id="outlined-basic" placeholder="Option 1" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option32} onChange={(e) => setoption32(e.target.value)}  id="outlined-basic" placeholder="Option 2" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option33} onChange={(e) => setoption33(e.target.value)}  id="outlined-basic" placeholder="Option 3" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option34} onChange={(e) => setoption34(e.target.value)}  id="outlined-basic" placeholder="Option 4" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Correct Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <select value={answer3} onChange={(e) => setanswer3(e.target.value)}  class="form-select" id="fl-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <Button onClick={() => saveQuiz(question3, option31, option32, option33, option34, answer3)} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                            Save Question
                                        </Button>
                                    </div>
                                    <hr />
                                    <div style={{ color: '#000' }} className="title">
                                        <h1 style={{ float: 'left !important' }}>
                                            Question 4
                                        </h1>
                                        <div className="form-floating mb-3">
                                            <TextField value={question4} onChange={(e) => setquestion4(e.target.value)} id="outlined-basic" placeholder="Question 4" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <TextField value={option41} onChange={(e) => setoption41(e.target.value)}  id="outlined-basic" placeholder="Option 1" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option42} onChange={(e) => setoption42(e.target.value)}  id="outlined-basic" placeholder="Option 2" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option43} onChange={(e) => setoption43(e.target.value)}  id="outlined-basic" placeholder="Option 3" fullWidth variant="outlined" />
                                        </div>
                                        <div className="form-floating mb-3">
                                            <TextField value={option44} onChange={(e) => setoption44(e.target.value)}  id="outlined-basic" placeholder="Option 4" fullWidth variant="outlined" />
                                        </div>
                                        <h2>
                                            Correct Option
                                        </h2>
                                        <div className="form-floating mb-3">
                                            <select value={answer4} onChange={(e) => setanswer4(e.target.value)}  class="form-select" id="fl-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <Button onClick={() => saveQuiz(question4, option41, option42, option43, option44, answer4)} startIcon={<SaveIcon />} fullWidth size="large" variant="contained" style={{ backgroundColor: 'green', color: '#fff', marginBottom: '4px' }}>
                                            Save Question
                                        </Button>
                                    </div>
                                    <br />
                                </section>
                            </Paper>
                        </Grid>
                    </Grid>
                    </div>
                </div>
                </main>
        </>
    )
}

export default QuizQuestions
