import React from 'react';
import Header from './static/Header';
import Footer from './static/Footer';

const Home = () => {
    return (
        <>
            <main className="page-wrapper">
                <Header />
                <section className="position-relative bg-secondary pt-5 pt-lg-7 pb-7 overflow-hidden">
                    <div className="shape shape-bottom shape-curve bg-body">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 185.4">
                        <path fill="currentColor" d="M3000,0v185.4H0V0c496.4,115.6,996.4,173.4,1500,173.4S2503.6,115.6,3000,0z"></path>
                    </svg>
                    </div>
                    <div className="container pt-3 pb-4 pt-lg-0">
                    <div className="row align-items-center">
                        <div className="col-lg-5 mt-lg-n7 pb-5 mb-sm-3 mb-lg-0 pb-lg-0 text-center text-lg-start">
                        <h1 className="display-4">Promote beyond the reach</h1>
                        <p className="pb-2">We will help to spread your wildest ideas to life.</p><a className="btn-video btn-video-primary me-3" href="93641234.html" data-sub-html="&lt;h6 className=&quot;fs-sm text-light&quot;&gt;Video caption&lt;/h6&gt;"></a><span className="fs-sm text-muted">Watch who, how and where</span>
                        </div>
                        <div className="col-lg-7">
                        <div className="parallax mx-auto" style={{ maxWidth: '705px' }}>
                            <div className="parallax-layer position-relative" data-depth="0.1"><img src="img/demo/creative-agency/parallax/layer01.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.35"><img src="img/demo/creative-agency/parallax/layer02.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.2"><img src="img/demo/creative-agency/parallax/layer03.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.5"><img src="img/demo/creative-agency/parallax/layer04.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.6"><img src="img/demo/creative-agency/parallax/layer05.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.4"><img src="img/demo/creative-agency/parallax/layer06.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.25"><img src="img/demo/creative-agency/parallax/layer07.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.35"><img src="img/demo/creative-agency/parallax/layer08.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.6"><img src="img/demo/creative-agency/parallax/layer09.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.45"><img src="img/demo/creative-agency/parallax/layer10.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.7"><img src="img/demo/creative-agency/parallax/layer11.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.3"><img src="img/demo/creative-agency/parallax/layer12.svg" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.15"><img src="img/demo/creative-agency/parallax/layer13.png" alt="Layer" /></div>
                            <div className="parallax-layer" data-depth="0.9"><img src="img/demo/creative-agency/parallax/layer14.svg" alt="Layer" /></div>
                        </div>
                        </div>
                    </div>
                    </div>
                </section>
                <section className="container position-relative zindex-5" style={{ marginTop: '-157px' }}>
                    <div className="bg-light rounded-3 shadow py-5 py-md-6 px-4 px-md-0">
                    <div className="row align-items-center py-3 py-md-0">
                        <div className="col-xl-6 col-lg-5 col-md-4 text-center">
                        <h2 className="pb-3 pb-md-0"><span className="d-block text-body opacity-25">Something</span>About Us</h2>
                        </div>
                        <div className="col-xl-5 col-lg-6 col-md-7 text-center text-md-start">
                        <p>Find aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum beatae vitae dicta sunt explicabo.</p>
                        <p className="mb-0">Ornare aenean euismod elementum nisi quis. Et ultrices neque ornare aenean euismod elementum nisi.</p>
                        </div>
                    </div>
                    </div>
                </section>
                
                <section className="container pt-5 pb-4 py-md-6 pt-lg-7">
                    <h2 className="text-center pt-3">Services</h2>
                    <p className="d-block fs-sm text-muted text-center mx-auto pb-4 mb-5" style={{ maxWidth: '400px' }}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis eum modi, adipisci facilis.</p>
                    <div className="row">
                    <div className="col-lg-2 offset-lg-1 col-sm-3 col-6 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-3" src="img/demo/creative-agency/services/01.svg" alt="Strategy" width="74" />
                        <h3 className="h5">COUPONS & CODES</h3>
                    </div>
                    <div className="col-lg-2 offset-lg-1 col-sm-3 col-6 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-3" src="img/demo/creative-agency/services/02.svg" alt="Digital" width="85" />
                        <h3 className="h5">QUIZZES</h3>
                    </div>
                    <div className="col-lg-2 offset-lg-1 col-sm-3 col-6 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-3" src="img/demo/creative-agency/services/03.svg" alt="Visual Identity" width="78" />
                        <h3 className="h5">GAMES</h3>
                    </div>
                    <div className="col-lg-2 offset-lg-1 col-sm-3 col-6 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-3" src="img/demo/creative-agency/services/04.svg" alt="Motion" width="83" />
                        <h3 className="h5">CONTESTS</h3>
                    </div>
                    <div className="col-lg-2 offset-lg-1 col-sm-3 col-6 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-3" src="img/demo/creative-agency/services/04.svg" alt="Motion" width="83" />
                        <h3 className="h5">GIVEAWAYS</h3>
                    </div>
                    </div>
                </section>
                <div className="bg-secondary" style={{ marginTop: '-300px', paddingTop: '300px' }}></div>
                </main>
                <Footer />
        </>
    )
}

export default Home
