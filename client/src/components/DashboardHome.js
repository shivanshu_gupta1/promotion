import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Profile from './static/Profile';

const DashboardHome = () => {
    const uid = sessionStorage.getItem("userId");
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };

    const [campaignName, setcampaignName] = React.useState('');

    const createCampaign = () => {
        var uploadData = {
            campaignName,
            userId: uid
        }
        axios.post(`${API_SERVICE}/api/v1/main/creatanewcampaign`, uploadData)
            .then((res) => {
                if (res.status === 200) {
                    window.location.href = `/marketing-tool-email?q=${res.data._id}`;
                } else if (res.status === 201) {
                    alert("Name already exist");
                }
            }).catch(err => console.log(err));
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="sm"
            >
                <DialogTitle style={{ color: 'black' }} id="alert-dialog-title">Create an email</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: 'black' }} id="alert-dialog-description">
                Keep your subscribers engaged by sharing your latest news, promoting a line of products, or announcing an event.
                </DialogContentText>
                    <div class="form-floating mb-3">
                        <input onChange={(e) => setcampaignName(e.target.value)} style={{ color: 'black' }} class="form-control" type="text" id="fl-text" placeholder="Campaign Name" />
                        <label for="fl-text">Campaign Name</label>
                    </div>
                    <center>
                        <h6>
                        OR
                        </h6>
                        <a className="mt-2 mb-2" href="/marketing-tool-allcampaign">
                            Show All Campaign
                        </a>
                    </center>
                    
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                <Button onClick={createCampaign} color="primary" autoFocus>
                    Submit
                </Button>
                </DialogActions>
            </Dialog>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">Solutions</h1>
                            

                            </div>
                            <div class="row no-gutters mx-n2 mb-4">
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img alt="Coupons" src="https://img.icons8.com/fluent/96/000000/redeem.png"/></center>
                                    <div class="card-body"><h2>Coupons Marketing</h2>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={() => window.location.href = "/marketing-tool-dashboard"} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img src="https://img.icons8.com/fluent/96/000000/email-open.png"/></center>
                                    <div class="card-body"><h2>Email Marketing</h2>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={handleClickOpen} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img src="https://img.icons8.com/fluent/94/000000/poster.png"/></center>
                                    <div class="card-body"><h2>Direct Mail Ads</h2>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={() => window.open(`/marketing-tool-posters`, '_blank', 'location=yes,height=900,width=1200,scrollbars=yes,status=yes')} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img src="https://img.icons8.com/fluent/96/000000/post-ads.png"/></center>
                                    <div class="card-body"><h5>Google/ Facebook Ads</h5>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={() => window.open(`/marketing-tool-ads`, '_blank', 'location=yes,height=900,width=1200,scrollbars=yes,status=yes')} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img className="mt-2" src="https://img.icons8.com/fluent/96/000000/new-message.png"/></center>
                                    <div class="card-body"><h2>Text SMS</h2>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={() => window.open(`/marketing-tool-message`, '_blank', 'location=yes,height=900,width=1200,scrollbars=yes,status=yes')} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 px-2 mb-3">
                                    <div class="card card-product card-hover"><center><img className="mt-2" src="https://img.icons8.com/fluent/96/000000/test.png"/></center>
                                    <div class="card-body"><h2>Quiz Maker</h2>
                                    </div>
                                    <center className="mb-4 p-2">
                                        <button onClick={() =>  window.location.href = "/marketing-tool-quiz"} style={{ width: '100%' }} className="btn btn-success">
                                            Select
                                        </button>
                                    </center>
                                    </div>
                                </div>
                            </div>
                            <div class="d-sm-flex align-items-center text-center text-sm-start">
                            <h6 class="text-nowrap my-2 me-3">Share this list:</h6><a class="btn-social bs-facebook me-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter me-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-email me-2 my-2" href="#"><i class="ai-mail"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

export default DashboardHome