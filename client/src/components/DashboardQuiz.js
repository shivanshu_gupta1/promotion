import React from 'react';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { v4 as uuidv4 } from 'uuid';
import Profile from './static/Profile';

const QuizList = ({ quiz }) => {
    var name = quiz.title;
    // name = name.substring(0, 10);
    // Remove HTML Elements from string
    name = name.replace( /(<([^>]+)>)/ig, '');
    // Remove Icons from string
    name = name.replace(/[^\x20-\x7E]/g, '');
    return (
        <div class="d-flex justify-content-between align-items-center fs-sm py-2 border-bottom">
            <div class="d-flex align-items-start py-1">
                <div class="ps-1">
                <h4>
                    {name}
                </h4>
                </div>
            </div>
            <span>
                <a href={`/marketing-tool-viewquiz?i=${quiz.quizId}`}>
                    View
                </a>    
            </span>
        </div>
    )
}

const DashboardQuiz = () => {
    const uid = sessionStorage.getItem("userId");

    const [allQuiz, setallQuiz] = React.useState([]);
    const [title, settitle] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };


    React.useEffect(() => {
        axios.get(`${API_SERVICE}/api/v1/main/findallquiz/${uid}`)
            .then(response => {
                setallQuiz(response.data);
            })
    }, []);

    const showPromotionsAll = () => {
        return allQuiz.map(quiz => {
            return <QuizList quiz={quiz} key={quiz._id} />
        })
    }

    const createQuiz = () => {
        var quizId = uuidv4() + Date.now();
        var uploadData = {
            title,
            quizId,
            userId: uid
        }
        axios.post(`${API_SERVICE}/api/v1/main/createnewquiz`, uploadData)
            .then((res) => {
                if (res.status === 200) {
                    window.location.href = `/marketing-tool-createquiz?q=${res.data.quizId}`;
                } else if (res.status === 201) {
                    alert("Already exist Please Try Again");
                }
            }).catch(err => console.log(err));
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
                maxWidth="sm"
            >
                <DialogTitle id="alert-dialog-title">New Quiz</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <div className="form-floating mb-3">
                        <input value={title} onChange={(e) => settitle(e.target.value)} className="form-control" type="text" id="fl-text" placeholder="Quiz Title" />
                        <label for="fl-text">Quiz Title</label>
                    </div>
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                <Button onClick={createQuiz} color="primary" autoFocus>
                    Proceed
                </Button>
                </DialogActions>
            </Dialog>
            <main class="page-wrapper">
                <header className="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky" data-scroll-header="" data-fixed-element="">
                    <div className="container px-0 px-xl-3">
                    <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4 text-light" href="/marketing-tool-dashboardhome">
                        <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" alt="Biz Promo" />
                    </a>
                    <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                        <div className="offcanvas-cap navbar-shadow">
                        <h5 className="mt-1 mb-0">Menu</h5>
                        <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                    </div>
                    </div>
                </header>
                <div class="position-relative bg-gradient" style={{ height: '480px' }}>
                    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 3000 260">
                        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                    </svg>
                    </div>
                </div>
                <div class="container position-relative zindex-5 pb-4 mb-md-3" style={{ marginTop: '-350px' }}>
                    <div class="row">
                    <Profile />
                    <div class="col-lg-8">
                        <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                        <div class="py-2 p-md-3">
                            <div class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">All Quiz</h1>
                            <a href="#!" onClick={handleClickOpen} className="btn btn-success float-right">Create New Quiz</a>
                            </div>
                            <div class="row no-gutters mx-n2 mb-4">
                            <div class="card">
                            <div class="card-body">
                                {showPromotionsAll()}
                            </div>
                            </div>
                            </div>
                            <div class="d-sm-flex align-items-center text-center text-sm-start">
                            <h6 class="text-nowrap my-2 me-3">Share this list:</h6><a class="btn-social bs-facebook me-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter me-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-email me-2 my-2" href="#"><i class="ai-mail"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </main>
        </>
    )
}

// 437545507453292
// 9e64cb70cee29dabd2d438e726266311
// kinyadaf1@neighborhoodeals.com
// Jennasmom2001
export default DashboardQuiz