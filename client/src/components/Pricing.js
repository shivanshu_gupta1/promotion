import React from 'react';
import Header from './static/Header';
import Footer from './static/Footer';

const Pricing = () => {
    return (
        <>
            <main className="page-wrapper">
                <Header />
                <div class="pricing-wrap container">
                    <h2 class="mb-5 mt-5 text-center">Pricing</h2>
                    <div class="row mb-5">
                    <div class="col-lg-4 col-sm-6 mb-grid-gutter">
                        <div class="card border-0 shadow w-100">
                        <div class="card-img-top bg-secondary text-center py-5 px-grid-gutter">
                            <h3 class="text-body mb-0">Free</h3>
                        </div>
                        <div class="card-body px-grid-gutter py-grid-gutter">
                            <div class="d-flex align-items-end py-2 px-4 mb-4">
                            <span class="h2 fw-normal text-muted mb-1 me-2">$</span>
                            <span class="price display-2 fw-normal text-primary px-1 me-2" data-current-price="0" data-new-price="0">0</span>
                            <span class="h3 fs-lg fw-medium text-muted mb-2">per<br />month</span>
                            </div>
                            <ul class="list-unstyled py-2 mb-4">
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>20 millions tracks</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Shuffle play</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-x fs-xl text-muted me-2"></i>
                                <span class="text-muted">No ads</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-x fs-xl text-muted me-2"></i>
                                <span class="text-muted">Get unlimited skips</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-x fs-xl text-muted me-2"></i>
                                <span class="text-muted">Offline mode</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-x fs-xl text-muted me-2"></i>
                                <span class="text-muted">7 profiles</span>
                            </li>
                            </ul>
                            <div class="text-center mb-2">
                            <a class="btn btn-outline-primary" href="#">Get started</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-grid-gutter">
                        <div class="card card-active w-100">
                        <div class="card-img-top bg-gradient text-center py-5 px-grid-gutter">
                            <h3 class="text-light mb-0">Premium</h3>
                        </div>
                        <div class="card-body px-grid-gutter py-grid-gutter">
                            <div class="d-flex align-items-end py-2 px-4 mb-4">
                            <span class="h2 fw-normal text-muted mb-1 me-2">$</span>
                            <span class="price display-2 fw-normal text-primary px-1 me-2" data-current-price="10" data-new-price="8">250</span>
                            <span class="h3 fs-lg fw-medium text-muted mb-2">per<br />month</span>
                            </div>
                            <ul class="list-unstyled py-2 mb-4">
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>20 millions tracks</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Shuffle play</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>No ads</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Get unlimited skips</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Offline mode</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>7 profiles</span>
                            </li>
                            </ul>
                            <div class="text-center mb-2">
                            <a class="btn btn-primary" href="#">Get started</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-grid-gutter">
                        <div class="card border-0 shadow w-100">
                        <div class="card-img-top bg-secondary text-center py-5 px-grid-gutter">
                            <h3 class="text-body mb-0">Family</h3>
                        </div>
                        <div class="card-body px-grid-gutter py-grid-gutter">
                            <div class="d-flex align-items-end py-2 px-4 mb-4">
                            <span class="h2 fw-normal text-muted mb-1 me-2">$</span>
                            <span class="price display-2 fw-normal text-primary px-1 me-2" data-current-price="15" data-new-price="12">300</span>
                            <span class="h3 fs-lg fw-medium text-muted mb-2">per<br />month</span>
                            </div>
                            <ul class="list-unstyled py-2 mb-4">
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>20 millions tracks</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Shuffle play</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>No ads</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Get unlimited skips</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>Offline mode</span>
                            </li>
                            <li class="d-flex align-items-center mb-3">
                                <i class="ai-check fs-xl text-primary me-2"></i>
                                <span>7 profiles</span>
                            </li>
                            </ul>
                            <div class="text-center mb-2">
                            <a class="btn btn-outline-primary" href="#">Get started</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>
                <Footer />
            </main>
        </>
    )
}

export default Pricing
