import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import parse from 'html-react-parser';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import browserInfo from 'browser-info';
import QRCode from "react-qr-code";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      backgroundRepeat: 'no-repeat',
      backgroundSize: '100%'
    },
}));

const minuteSeconds = 60;
const hourSeconds = 3600;
const daySeconds = 86400;

const timerProps = {
  isPlaying: true,
  size: 120,
  strokeWidth: 6
};

const renderTime = (dimension, time) => {
  return (
    <div className="time-wrapper">
      <div className="time">{time}</div>
      <div>{dimension}</div>
    </div>
  );
};

const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
const getTimeDays = (time) => (time / daySeconds) | 0;



const QuestionList = ({ quizQuestion, i }) => {
    
    return (
        <div className="card mt-2">
        <div className="card-body">
            <h2 className="card-title">
               Question {quizQuestion.question}
            </h2>
            <div class="form-check">
                <input class="form-check-input" type="radio" id={`ex-radio-${i}1`} name={`ex-radio-${i}1`} />
                <label class="form-check-label" for={`ex-radio-${i}1`}>{quizQuestion.option1}</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id={`ex-radio-${i}2`} name={`ex-radio-${i}2`} />
                <label class="form-check-label" for={`ex-radio-${i}2`}>{quizQuestion.option2}</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id={`ex-radio-${i}3`} name={`ex-radio-${i}3`} />
                <label class="form-check-label" for={`ex-radio-${i}3`}>{quizQuestion.option3}</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id={`ex-radio-${i}4`} name={`ex-radio-${i}4`} />
                <label class="form-check-label" for={`ex-radio-${i}4`}>{quizQuestion.option4}</label>
            </div>
        </div>
        <br />
        <hr />
        </div>

    )
}



const PublishQuiz = ({ location }) => {
    const classes = useStyles();
    const stratTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = stratTime + 243248; // use UNIX timestamp in seconds
    const remainingTime = endTime - stratTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;
    const [quizId, setquizId] = React.useState('');
    const [steps, setsteps] = React.useState(1);

    const [firstname, setfirstname] = React.useState('');
    const [lastname, setlastname] = React.useState('');
    const [email, setemail] = React.useState('');
    const [password, setpassword] = React.useState('');
    const [quizQuestions, setquizQuestions] = React.useState([]);


    React.useEffect(() => {
        const { s } = queryString.parse(location.search);
        axios.get(`${API_SERVICE}/api/v1/main/fetchquiz/${s}`)
            .then((d) => {
                settitle(d.data[0].title);
                setdescription(d.data[0].description);
                setlabel(d.data[0].buttonlabel);
                setregistrationDescription(d.data[0].registration_description);
            }).catch(err => console.log(err));

        axios.get(`${API_SERVICE}/api/v1/main/fetchquizquestion/${s}`)
            .then((response) => {
                setquizQuestions(response.data);
            }).catch(err => console.log(err));
        setquizId(s);
    }, []);

    const [title, settitle] = React.useState('✅ Enter your code and [TRY YOUR LUCK/ENTER THE PRIZE DRAW] 🍀');
    const [label, setlabel] = React.useState('Label');
    const [registrationDescription, setregistrationDescription] = React.useState('Label');
    const [description, setdescription] = React.useState(`
    <h3>Do you want to win a [DESCRIPTION OF PRIZE] valued at [PRICE]? </h3>
    
    ➡️ Follow these steps to participate:
    
    <ul><li>✏️Identify yourself and fill in the <strong>registration form</strong>.</li>
    <li>✅ <strong>Enter the code</strong> in the corresponding field to validate it.</li>
    <li>👍 Submit the form to finalize and you'll be in the [prize draw]</li></ul> 
    
    🗓️ Promotion valid until [INSERT DATES]`);

    const register = () => {
        var browser = browserInfo();
        var uploadData = {
            quizId,
            firstname,
            lastname,
            email,
            password,
            browser
        }
        axios.post(`${API_SERVICE}/api/v1/main/registeruserquiz`, uploadData)
            .then(() => {
                setsteps(3);
            }).catch(err => console.log(err));
    }

    const showQuiz = () => {
        var i = 0;
        return quizQuestions.map(quizQuestion => {
            i = i + 1;
            return <QuestionList i={i} quizQuestion={quizQuestion} key={quizQuestion._id} />
        })
    }

    return (
        <div className="container center promotionpublish">
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    {
                        steps === 1 ? (
                            <Paper className={classes.paper}>
                                <center>
                                    <img className="imagebanner" src="https://www.bragitoff.com/wp-content/uploads/2015/03/quiz.jpg" />
                                </center>
                                <div style={{ color: '#000' }} className="title">
                                    {parse(`
                                        ${title}
                                    `)}
                                    <br />
                                    {parse(`
                                        ${description}
                                    `)}
                                </div>
                                <br />
                                <Button onClick={() => setsteps(2)} fullWidth size="large" variant="contained" color="primary">
                                    {label}
                                </Button>
                                <br />
                                <br />
                                <div className="countdown">
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#7E2E84"]]}
                                        duration={daysDuration}
                                        initialRemainingTime={remainingTime}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("days", getTimeDays(daysDuration - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#D14081"]]}
                                        duration={daySeconds}
                                        initialRemainingTime={remainingTime % daySeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > hourSeconds
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("hours", getTimeHours(daySeconds - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#EF798A"]]}
                                        duration={hourSeconds}
                                        initialRemainingTime={remainingTime % hourSeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > minuteSeconds
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("minutes", getTimeMinutes(hourSeconds - elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                    <CountdownCircleTimer
                                        {...timerProps}
                                        colors={[["#218380"]]}
                                        duration={minuteSeconds}
                                        initialRemainingTime={remainingTime % minuteSeconds}
                                        onComplete={(totalElapsedTime) => [
                                        remainingTime - totalElapsedTime > 0
                                        ]}
                                    >
                                        {({ elapsedTime }) =>
                                        renderTime("seconds", getTimeSeconds(elapsedTime))
                                        }
                                    </CountdownCircleTimer>
                                </div>
                                <br />
                                <br />
                                <a href="#!">
                                    TERMS & CONDITIONS
                                </a>
                            </Paper>
                        ) : steps === 2 ? (
                            <Paper className={classes.paper}>
                                <section>
                                    <center>
                                        <img className="imagebanner" src="https://www.bragitoff.com/wp-content/uploads/2015/03/quiz.jpg" />
                                    </center>
                                    <br />
                                    <h4 style={{ float: 'left' }}>First Name</h4>
                                    <TextField onChange={(e) => setfirstname(e.target.value)} fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Last Name</h4>
                                    <TextField onChange={(e) => setlastname(e.target.value)} fullWidth id="outlined-basic" type="text" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Email</h4>
                                    <TextField onChange={(e) => setemail(e.target.value)} fullWidth id="outlined-basic" type="email" variant="outlined" />
                                    <br />
                                    <h4 style={{ float: 'left' }}>Password</h4>
                                    <TextField onChange={(e) => setpassword(e.target.value)} fullWidth id="outlined-basic" type="password" variant="outlined" />
                                    <br />
                                    <br />
                                    <Button onClick={register} fullWidth size="large" variant="contained" color="primary">
                                        Register
                                    </Button>
                                    <br />
                                    <br />
                                    <div className="title">
                                        {parse(`
                                            ${registrationDescription}
                                        `)}
                                    </div>
                                </section>
                            </Paper>
                        ) : steps === 3 ? (
                            <Paper className={classes.paper}>
                                <section style={{ textAlign: 'left' }} >
                                    <center>
                                        <img className="imagebanner" src="https://www.bragitoff.com/wp-content/uploads/2015/03/quiz.jpg" />
                                    </center>
                                    {showQuiz()}
                                    <br />
                                    <Button onClick={() => setsteps(4)} size="large" variant="contained" fullWidth color="primary">Submit</Button>
                                </section>
                            </Paper>
                        ) : steps === 4 ? (
                            <Paper className={classes.paper}>
                                <section>
                                    <center>
                                        <img className="imagebanner" src="https://www.bragitoff.com/wp-content/uploads/2015/03/quiz.jpg" />
                                    </center>
                                    <div className="title">
                                        {parse(`
                                            <h1>Thank you for participating!</h1>
                                            <strong>
                                            You can share with your friends so they can enter too. 😀
                                            </strong>
                                        `)}
                                    </div>
                                    <br />
                                    <center>
                                        <QRCode value={quizId} />
                                    </center>
                                    <br />
                                    <Button target="_blank" href="https://www.kaspersky.com/content/en-global/images/repository/isc/2020/9910/a-guide-to-qr-codes-and-how-to-scan-qr-codes-2.png" fullWidth size="large" variant="contained" color="secondary">
                                        Download
                                    </Button>
                                </section>
                            </Paper>
                        ) : null
                    }
                    
                </Grid>
            </Grid>
        </div>
    )
}

export default PublishQuiz
