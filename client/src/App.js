import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Components
// Merketing Tool
import Home from './components/Home';
import SignIn from './components/SignIn';
import Signup from './components/Signup';
import DashboardPromotion from './components/DashboardPromotion';
import DashboardHome from './components/DashboardHome';
import CreatePromotion from './components/CreatePromotion';
import DesignPromotion from './components/DesignPromotion';
import LoginDesignPromotion from './components/LoginDesignPromotion';
import RegistrationDesignPromotion from './components/RegistrationDesignPromotion';
import ThankyouDesignPromotion from './components/ThankyouDesignPromotion';
import DatesDesignPromotion from './components/DatesDesignPromotion';
import LeagalContentDesignPromotion from './components/LeagalContentDesignPromotion';
import ShareDesignPromotion from './components/ShareDesignPromotion';
import PublishPromo from './components/PublishPromo';
import Viewpromotion from './components/Viewpromotion';
import Solutions from './components/Solutions';
import Pricing from './components/Pricing';
import Contact from './components/Contact';
import DesignEmail from './components/DesignEmail';
import Email from './components/Email';
import AllEmail from './components/AllEmail';
import ViewEmail from './components/ViewEmail';
import Posters from './components/Posters';
import Ads from './components/Ads';
import Message from './components/Message';
import DashboardQuiz from './components/DashboardQuiz';
import DesignQuiz from './components/DesignQuiz';
import QuizQuestions from './components/QuizQuestions';
import RegistrationQuiz from './components/RegistrationQuiz';
import ShareQuiz from './components/ShareQuiz';
import PublishQuiz from './components/PublishQuiz';
import Viewquiz from './components/Viewquiz';

// Main Website
import HomeMain from './components/Main/Home';
import Registerbusiness from './components/Main/Registerbusiness';
import Thankyou from './components/Main/Thankyou';

function App() {
  return (
    <>
      <Router>
        <Route path="/" exact component={HomeMain} />
        <Route path="/register" exact component={Registerbusiness} />
        <Route path="/thankyou" exact component={Thankyou} />
        <Route path="/signin" exact component={SignIn} />

        
        <Route path="/marketing-tool/" exact component={Home} />
        <Route path="/marketing-tool-ads" exact component={Ads} />
        <Route path="/marketing-tool-message" exact component={Message} />
        <Route path="/marketing-tool-posters" exact component={Posters} />
        <Route path="/marketing-tool-quiz" exact component={DashboardQuiz} />
        <Route path="/marketing-tool-questions" exact component={QuizQuestions} />
        <Route path="/marketing-tool-createquiz" exact component={DesignQuiz} />
        <Route path="/marketing-tool-registrationquiz" exact component={RegistrationQuiz} />
        <Route path="/marketing-tool-sharequiz" exact component={ShareQuiz} />
        <Route path="/marketing-tool-signup" exact component={Signup} />
        <Route path="/marketing-tool-dashboardhome" exact component={DashboardHome} />
        <Route path="/marketing-tool-email" exact component={Email} />
        <Route path="/marketing-tool-view" exact component={ViewEmail} />
        <Route path="/marketing-tool-allcampaign" exact component={AllEmail} />
        <Route path="/marketing-tool-dashboard" exact component={DashboardPromotion} />
        <Route path="/marketing-tool-designemail" exact component={DesignEmail} />
        <Route path="/marketing-tool-createpromotion" exact component={CreatePromotion} />
        <Route path="/marketing-tool-designpromotion" exact component={DesignPromotion} />
        <Route path="/marketing-tool-logindesignpromotion" exact component={LoginDesignPromotion} />
        <Route path="/marketing-tool-registrationdesignpromotion" exact component={RegistrationDesignPromotion} />
        <Route path="/marketing-tool-thankyoudesignpromotion" exact component={ThankyouDesignPromotion} />
        <Route path="/marketing-tool-datesdesignpromotion" exact component={DatesDesignPromotion} />
        <Route path="/marketing-tool-leagaldesignpromotion" exact component={LeagalContentDesignPromotion} />
        <Route path="/marketing-tool-sharedesignpromotion" exact component={ShareDesignPromotion} />
        <Route path="/p" exact component={PublishPromo} />
        <Route path="/q" exact component={PublishQuiz} />
        <Route path="/marketing-tool-viewpromotion" exact component={Viewpromotion} />
        <Route path="/solutions" exact component={Solutions} />
        <Route path="/pricing" exact component={Pricing} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/marketing-tool-viewquiz" exact component={Viewquiz} />
      </Router>
    </>
  );
}

export default App;
